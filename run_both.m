close all;
clear;

addpath('1_mesh_gene');
addpath('2_preparation');
addpath('3_global_index');
addpath('4_form_matrix');
addpath('5_solve');
addpath('6_error');

global problems;
problems = cell(11,1);
problems{1} = 'a = 1, b = 0, u = x^6 + y^5';
problems{2} = 'a = 1, b = 0, u = exp(x+y)';
problems{3} = 'a = 1, b = 0, u = exp(x^2*y)';
problems{4} = 'a = 1, b = 0, u = sin(2*pi*x)*sin(pi*y)';
problems{5} = 'a = 1, b = 0, u = sin(x*y)';
problems{6} = 'a = 1, b = 0, u = exp(-x*y)';
problems{7} = 'a = 1, b = 0, u = log(0.1+x+y)';
problems{8} = 'a = 1, b = 0, u = tanh(x^2*y) + exp(x+y)';
problems{9} = 'a = x*y, b = 0, u = exp(x+y)';
problems{10} = 'a = exp(x)*y, b = cos(4*pi*x), u = sin(2*pi*x)*exp(x*y)';
problems{11} = 'a = sin(2*pi*x) + y + 1, b = 0, u = exp(x+y)';

disp('Solve for u');
fprintf('\n\tgrad . (a * grad(u)) + b*u = f   in [0, 1]^2\n\n');
for i = 1:length(problems)
    fprintf('    %d) %s\n',i,problems{i});
end
fprintf('\n');

global prob;
prob = input('problem number = ');

max_pol_deg = input('max polynomial degree = ');
meshtype=5;
%meshtype = input('meshtype =(4 for quadrilateral meshes, 5 for rectangular meshes)     ');
n = input('Number of iterations = ');

e_L2 = zeros(n,1);
en_L2 = cell(n,1);
e_H1 = zeros(n,1);
en_H1 = cell(n,1);
eEn_L2 = cell(n,1);
eEn_H1 = cell(n,1);
eE_L2 = zeros(n,1);
eE_H1 = zeros(n,1);
un_L2 = cell(n,1);
un_H1 = cell(n,1);
uapproxn_L2 = cell(n,1);
uapproxn_H1 = cell(n,1);
E_L2 = zeros(n,1);
En_L2 = cell(n,1);
E_H1 = zeros(n,1);
En_H1 = cell(n,1);
xplot = cell(n,1);
yplot = cell(n,1);
u_plot = cell(n,1);
uapprox_plot = cell(n,1);
e_plot = cell(n,1);
E_plot = cell(n,1);

x1 = 0;
x2 = 2;
y1 = 0;
y2 = 1;
dom = [x1 x2 y1 y2];

nx = 2;
ny = 2;
H = 1./2.^(1:n);
Nx = 2.^(1:n)*nx;
Ny = 2.^(1:n)*ny;
elems = (Nx.*Ny)';

for i = 1:n
    [en_L2{i},en_H1{i},En_L2{i},En_H1{i},eEn_L2{i},eEn_H1{i},un_L2{i},un_H1{i},uapproxn_L2{i},uapproxn_H1{i},xplot{i},yplot{i},u_plot{i},uapprox_plot{i},E_plot{i}] = main_file(max_pol_deg,meshtype,dom,Nx(i),Ny(i));
    
    e_L2(i) = norm(en_L2{i});
    e_H1(i) = norm(en_H1{i});
    E_L2(i) = norm(En_L2{i});
    E_H1(i) = norm(En_H1{i});
    eE_L2(i) = norm(eEn_L2{i}(2:end-1,2:end-1),'fro');
    eE_H1(i) = norm(eEn_H1{i}(2:end-1,2:end-1),'fro');
    e_plot{i} = u_plot{i} - uapprox_plot{i};
end

eff_L2 = E_L2./e_L2;
eff_H1 = E_H1./e_H1;

for i = 5
    figure;
    subplot(2,2,1);
    surf(xplot{i},yplot{i},u_plot{i});
    shading interp;
    title('$u$','Interpreter','Latex');
    xlabel('$x$','Interpreter','Latex');
    ylabel('$y$','Interpreter','Latex');
    zlabel('$u(x,y)$','Interpreter','Latex');
    subplot(2,2,2);
    surf(xplot{i},yplot{i},uapprox_plot{i});
    shading interp;
    title('$U$','Interpreter','Latex');
    xlabel('$x$','Interpreter','Latex');
    ylabel('$y$','Interpreter','Latex');
    zlabel('$U(x,y)$','Interpreter','Latex');
    subplot(2,2,3);
    surf(xplot{i},yplot{i},e_plot{i});
    shading interp;
    title('$e$','Interpreter','Latex');
    xlabel('$x$','Interpreter','Latex');
    ylabel('$y$','Interpreter','Latex');
    zlabel('$e(x,y)$','Interpreter','Latex');
    subplot(2,2,4);
    surf(xplot{i},yplot{i},E_plot{i});
    shading interp;
    title('$\hat{E}$','Interpreter','Latex');
    xlabel('$x$','Interpreter','Latex');
    ylabel('$y$','Interpreter','Latex');
    zlabel('$\hat{E}(x,y)$','Interpreter','Latex');
    
    x_v = linspace(x1,x2,Nx(i));
    y_v = linspace(y1,y2,Ny(i));
    figure;
    [xmsh,ymsh] = meshgrid(x_v,y_v);
    surf(xmsh,ymsh,reshape(en_H1{i},Nx(i),Ny(i))');
    title('$|\:\!\!|e|\:\!\!|_{1,n}$','Interpreter','Latex');
    xlabel('$x$','Interpreter','Latex');
    ylabel('$y$','Interpreter','Latex');
    zlabel('$|\:\!\!|e|\:\!\!|_{1,n}$','Interpreter','Latex');
    fname = sprintf('prob%d_en_norm_p%d_N%d',prob,max_pol_deg,elems(i));
    saveas(gcf,fname,'epsc');
    figure;
    surf(xmsh,ymsh,reshape(En_H1{i},Nx(i),Ny(i))');
    title('$|\:\!\!|\hat{E}|\:\!\!|_{1,n}$','Interpreter','Latex');
    xlabel('$x$','Interpreter','Latex');
    ylabel('$y$','Interpreter','Latex');
    zlabel('$|\:\!\!|\hat{E}|\:\!\!|_{1,n}$','Interpreter','Latex');
    fname = sprintf('prob%d_Ehatn_norm_p%d_N%d',prob,max_pol_deg,elems(i));
    saveas(gcf,fname,'epsc');
    
    x_v = linspace(x1,x2,Nx(i)+1);
    y_v = linspace(y1,y2,Ny(i)+1);
    ien_H1_v = 1./en_H1{i};
    iEn_H1_v = 1./En_H1{i};
    %cbmin = min([ien_H1_v; iEn_H1_v]);
    %cbmax = max([ien_H1_v; iEn_H1_v]);
    
    ien_H1 = reshape(ien_H1_v,Nx(i),Ny(i))';
    figure;
    pcolor(x_v,y_v,[[ien_H1 zeros(Ny(i),1)]; zeros(1,Nx(i)+1)]);
    set(gca,'YDir','normal');
    colorbar;
    %caxis([cbmin cbmax]);
    caxis([min(ien_H1_v) max(ien_H1_v)]);
    title('$|\:\!\!|e|\:\!\!|_{1,n}^{-1}$','Interpreter','Latex');
    xlabel('$x$','Interpreter','Latex');
    ylabel('$y$','Interpreter','Latex');
    fname = sprintf('prob%d_ien_norm_p%d_N%d',prob,max_pol_deg,elems(i));
    saveas(gcf,fname,'epsc');
    
    iEn_H1 = reshape(iEn_H1_v,Nx(i),Ny(i))';
    figure;
    pcolor(x_v,y_v,[[iEn_H1 zeros(Ny(i),1)]; zeros(1,Nx(i)+1)]);
    set(gca,'YDir','normal');
    colorbar;
    %caxis([cbmin cbmax]);
    caxis([min(iEn_H1_v) max(iEn_H1_v)]);
    title('$|\:\!\!|\hat{E}|\:\!\!|_{1,n}^{-1}$','Interpreter','Latex');
    xlabel('$x$','Interpreter','Latex');
    ylabel('$y$','Interpreter','Latex');
    fname = sprintf('prob%d_iEhatn_norm_p%d_N%d',prob,max_pol_deg,elems(i));
    saveas(gcf,fname,'epsc');
    
    TH1_v = En_H1{i}./en_H1{i};
    TH1 = reshape(TH1_v,Nx(i),Ny(i))';
	figure;
    pcolor(linspace(x1,x2,Nx(i)+1),linspace(y1,y2,Ny(i)+1),[[TH1 ones(Ny(i),1)]; ones(1,Nx(i)+1)]);
    set(gca,'YDir','normal')
    colorbar;
    caxis([min(TH1_v) max(TH1_v)]);
    title('$|\:\!\!|\hat{E}|\:\!\!|_{1,n} / |\:\!\!|e|\:\!\!|_{1,n}$','Interpreter','Latex');
    xlabel('$x$','Interpreter','Latex');
    ylabel('$y$','Interpreter','Latex');
    fname = sprintf('prob%d_thetan_p%d_N%d',prob,max_pol_deg,elems(i));
    saveas(gcf,fname,'epsc');
end

if length(H) > 1
    alpha = ORDER(e_L2,H);
    beta = ORDER(e_H1,H);
    alpha1 = ORDER(E_L2,H);
    beta1 = ORDER(E_H1,H);
    alpha2 = ORDER(eE_L2,H);
    beta2 = ORDER(eE_H1,H);
    disp1 = [elems e_L2 e_H1 E_L2 E_H1 eff_L2 eff_H1]';
    disp2 = [alpha beta];
    disp3 = [alpha1 beta1];
    disp4 = [alpha2 beta2];
    
    fprintf('elems    e_L2         e_H1         E_L2         E_H1         eff_L2   eff_H1\n')
    fprintf('%5d    %5.4e   %5.4e   %5.4e   %5.4e   %5.4f   %5.4f\n',disp1)
    
    fprintf('\nexact                    approx                   e - E\n');
    fprintf('L2_order   H1_order      L2_order   H1_order      L2_order   H1_order\n')
    fprintf('%5.4f     %5.4f        %5.4f     %5.4f        %5.4f     %5.4f\n',[disp2 disp3 disp4]')
end

latex_table(max_pol_deg,elems,e_H1,beta,E_H1,beta2,eff_H1);