function [en_L2,en_H1,En_L2,En_H1,eEn_L2,eEn_H1,un_L2,un_H1,uapproxn_L2,uapproxn_H1,xplot,yplot,u_plot,uapprox_plot,E_plot] = main_file(max_pol_deg,meshtype,dom,Nx,Ny)

%%%%%%%%%%%%%%%%%%%
% generate mesh
%%%%%%%%%%%%%%%%%%%
x1 = dom(1);
x2 = dom(2);
y1 = dom(3);
y2 = dom(4);
[p,e,t] = MESH_GENE([x1,y1],[x2,y1],[x2,y2],[x1,y2],Nx,Ny);

num_of_side = 4;
num_of_elem = size(t,2);
%num_of_side_node = size(e,2);
num_of_node = size(p,2);

bctype = 1; % only Dirichlet boundary condition considered
gauss_deg = max_pol_deg+2;
[xi,w] = gauss_quad(gauss_deg,-1,1);
[x_v,y_v] = gen_pt(num_of_elem,p,t,xi);
%[x_full,y_full] = gen_pt_full(x_v,y_v,num_of_elem);

%---------------------------------------------------
% evaluates the SF, dSF_dt for all the xi pts 
% from pol_deg=1,...,max_pol_deg, hence to speed up
%the computation
%---------------------------------------------------
SF_v = SF_vec(gauss_deg,max_pol_deg,xi);

dSF_dt_v = dSF_dt_vec(gauss_deg,max_pol_deg,xi);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% use FEM to solve equation   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% if meshtype=4
%     space == 2;
% else 
%     space =4;
% end
space = 2;

%---------------------------------
% generate the global index
%---------------------------------

nc = node_connection(num_of_elem,num_of_node,num_of_side,t);
max_connection = size(nc,1);
rc =r_connection(num_of_elem,num_of_side,nc,t);

rp = max_pol_deg*ones(num_of_elem,1);
rd = r_dof(rp,space) ;

[rci,rcw] = global_index(num_of_elem,num_of_node,t,rc,rp,space,bctype);
rci_full = global_index_full(num_of_elem,num_of_node,t,rc,rp,rci,space,bctype);

%t1=cputime-t0
%-------------------------------------------------------------
% compute the global stiffness matrix, mass matrix,load matrix
%-------------------------------------------------------------
u_0 = u_zero(num_of_elem,p,t,rc,rp,rd,rci_full,space);
[GA,GA1,GF] = FORM_GA_GF(x_v,y_v,SF_v,dSF_dt_v,num_of_elem,gauss_deg,p,t,rci,rp,rd,xi,w,space);
F = main_rhs(x_v,y_v,SF_v,dSF_dt_v,GF,num_of_elem,gauss_deg,p,t,rci,rci_full,u_0,rp,rd,xi,w,space);
A = GA+GA1;

u_c=u_0;
u_c(1:max(rci))=A\F;

%[u_h,u_exact] = u_full(num_of_elem,gauss_deg,xi,u_c,rci_full,p,t,rp,rd,x_v,y_v,space);

%t2=cputime-t0
%mesh(x_full,y_full,u_h)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% A Posteriori Error Estimation        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[en_L2,en_H1,un_L2,un_H1,uapproxn_L2,uapproxn_H1,xplot,yplot,u_plot,du_dx_plot,du_dy_plot,uapprox_plot,du_approxdx_plot,du_approxdy_plot] = error_L2H1(Nx,Ny,x_v,y_v,u_c,p,t,rci_full,rp,rd,gauss_deg,xi,w);

[En_L2,En_H1,E_plot,dE_dx_plot,dE_dy_plot] = error_E_e(dom,Nx,Ny,p,t,x_v,y_v,gauss_deg,max_pol_deg,SF_v,dSF_dt_v,xi,w,meshtype,rci_full,rp,rd,u_c);

e_plot = u_plot - uapprox_plot;
de_dx = du_dx_plot - du_approxdx_plot;
de_dy = du_dy_plot - du_approxdy_plot;
eEn_L2 = zeros(Nx,Ny);
eEn_H1 = zeros(Nx,Ny);
for i = 1:Nx
    for j = 1:Ny
        for b=1:gauss_deg
            for k=1:gauss_deg
                Corner(1:4,:) = p(:,t(1:4,(j-1)*Ny+Nx))';
                J = det_Jab(xi(b),xi(k),Corner(:,1)',Corner(:,2)');
                dJ = J(1,1)*J(2,2)-J(1,2)*J(2,1);
                
                xst = (i-1)*gauss_deg;
                yst = (j-1)*gauss_deg;
                eE = e_plot(xst+b,yst+k) - E_plot(xst+b,yst+k);
                eEn_L2(i,j) = eEn_L2(i,j) + w(b)*w(k)*eE^2*dJ;
                
                deE_dx = de_dx(xst+b,yst+k) - dE_dx_plot(xst+b,yst+k);
                deE_dy = de_dy(xst+b,yst+k) - dE_dy_plot(xst+b,yst+k);
                eEn_H1(i,j) = eEn_H1(i,j) + w(b)*w(k)*(deE_dx^2 + deE_dy^2)*dJ + w(b)*w(k)*eE^2*dJ;
            end
        end
    end
end

eEn_L2 = sqrt(eEn_L2);
eEn_H1 = sqrt(eEn_H1);