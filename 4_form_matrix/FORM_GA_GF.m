function [GA,GA1,GF] = FORM_GA_GF(x_v,y_v,SF_v,dSF_dt_v,num_of_elem,gauss_deg,p,t,rci,rp,rd,xi,w,space)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                                        
% fucntion [GA,GA1,GF] = FORM_GA_GF(SF_v,dSF_dt_v,num_of_elem,gauss_deg,p,r,rci,rcw,rp,ri,xi,w,type)          
%                                                                                                         
% Inputs:                                                                                                 
%  * SF_v:  which evaluates the SF for all the xi pts from pol_deg=1,...,max_pol_deg                      
%                                                                                                         
%  * dSF_dt_v:  which evaluates the dSF_dt for all the xi pts from pol_deg=1,...,max_pol_deg              
%                                                                                                         
%  * num_of_elem: number of element in the mesh                                                           
%                                                                                                         
%  * gauss_deg: order of Gaussian quadrature                                                              
%                                                                                                         
%  * p: point matrix of size 2 x number of points                                                         
%        row 1: x-coordinates of points                                                                   
%        row 2: y-coordinates of points                                                                   
%                                                                                                         
%  * t: is the index of the corner vertex on element K_j,j=1,...,num_of_elem                                                                                                                                       %
%  
%  *rci: which stores the global index value of the interior node for each element K_j,1,...,num_of_elem  
%         e.g the global index of the element 2 ,3 node =rci(ri(2)+3)                                      
%                                                                                                         
%  *rcw: which stores the direction of the basis function for each element K_j,1,...,num_of_elem                                                                                                                   %
%  
%  *rp: is a vector which stores the pol degree of each element K_j,j=1,...,num_of_elem                   
%                                                                                                         
%  *rd: which store the aggreciate dof of the elemnet                                                     
%       hence , # of local dof of element i = rd(i+1)-rd(i);                                              
%                                                                                                         
%  * xi: Gaussian integration points                                                                      
%                                                                                                         
%  * w : Gaussian weight  points                                                                          
%                                                                                                         
%  *meshtype (type of element) : triangle = 3                                                                 
%                            quad     = 4                                                                 
%                                                                                                         
% Outputs:                                                                                                
%                                                                                                         
%  * GA1: global mass matrix                                                                               
%  * GA: global stiffness matrix                                                                          
%  * GF: global load vector                                                                               
%                                                                                                         
%                                                                      
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initialise matrix GA and GA1     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
total_dof=max(rci);
GA = sparse(total_dof,total_dof); % in general, the size of stiffness matrix will be GA_(total_dofxtotal_dof) 
GA1 = sparse(total_dof,total_dof);
GF = sparse(total_dof,1);                                     
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% main program                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                                        
for l = 1:num_of_elem
   
   %i1=r(1,l);         % index of first nodal point in element l             i4  ____________  i3           
   %i2=r(2,l);          % index of second nodal point in element l              /    Kj      /              
   %i3=r(3,l);          % index of third nodal point in element l              /____________/        
   %i4=r(4,l);          % index of fourth nodal point in element l           i1              i2
   
   
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   % constructs element stiffness matrix and mass matrix     %
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   %Corner=[p(:,i1)';p(:,i2)';p(:,i3)';p(:,i4)'];
   for k=1:4
        Corner(k,:)=p(:,t(k,l))';
   end
   [EA, EA1,EF] = FORM_EA_EF(x_v,y_v,l,Corner,SF_v,dSF_dt_v,rp(l),gauss_deg,xi,w,space);

   
   
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   % assemble global stiffness matrix and mass matrix        %
   % using Index_golbal function                             %
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   
  
   num_shape=rd(l+1)-rd(l);
         
   for i=1:num_shape
      if (rci(rd(l)+i)>0)
        GF(rci(rd(l)+i),1)=GF(rci(rd(l)+i),1)+EF(i);
        %GL(rci(ri(l)+j),1)=GL(rci(ri(l)+j),1)+EL(j);
       end
       for j=1:num_shape
            if (rci(rd(l)+i)>0)&(rci(rd(l)+j)>0)
                GA(rci(rd(l)+i),rci(rd(l)+j))=GA(rci(rd(l)+i),rci(rd(l)+j))+EA(i,j);
                GA1(rci(rd(l)+i),rci(rd(l)+j))=GA1(rci(rd(l)+i),rci(rd(l)+j))+EA1(i,j);
            end
       end
   end   
end