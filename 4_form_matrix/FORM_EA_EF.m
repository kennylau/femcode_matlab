function [EA,EA1,EF] = FORM_EA_EF(x_v,y_v,elem,Corner,SF_v,dSF_dt_v,pol_deg,gauss_deg,xi,w,space)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                                            
% fucntion [EA,EA1,EF] = FORM_EA_EF(Corner,SF_v,dSF_dt_v,pol_deg,gauss_deg,xi,w,ttype,type)                      
%                                                                                                            
% Inputs:                                                                                                    
%  * Corner: which stores the x and y coordinates of the vertex nodes of the element K_j                     
%            , i.e.  Corner=  [X1 X2 X3 X4;Y1 Y2 Y3 Y4]  for quad                                            
%                                                                                                            
%  * SF_v:  which evaluates the SF for all the xi pts from pol_deg=1,...,max_pol_deg                         
%                                                                                                            
%  * dSF_dt_v:  which evaluates the dSF_dt for all the xi pts from pol_deg=1,...,max_pol_deg                 
%                                                                                                            
%  * pol_deg : polynomial order in element K_j                                                               
%                                                                                                            
%  * gauss_deg: order of Gaussian quadrature                                                                 
%                                                                                                            
%  * xi: Gaussian integration points                                                                         
%                                                                                                            
%  * w : Gaussian weight  points                                                                             
%                                                                                                            
%  * ttype = mapping type i.e                                                                                
%            (1)   linear,                                                                                   
%            (2)   quadratic parametric mapping (need convensional shape function)                           
%            (3) Blending function method (using in p_version generally)                                     
%                                                                                                            
%  *type (type of element) : triangle = 3                                                                    
%                            quad     = 4                                                                    
%                                                                                                            
% Outputs:                                                                                                   
%                                                                                                            
%  * EA: element stiffness matrix 
%  * EA1:element mass matrix
%  * EF: element load vector                                                                                  
%                                                                                                            
%                                                                           
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%
%  Initialise arrays    %
%%%%%%%%%%%%%%%%%%%%%%%%%
%

        elem_dof = basenum(pol_deg,space);
        EF  = zeros(elem_dof,1);
        EA =zeros(elem_dof,elem_dof);
        EA1 =zeros(elem_dof,elem_dof);

    
    % gnerate corresponding X1 and X2 to put in transformation
    % for the case of type=4
    % e.g. X1=[a(1) b(1) c(1) d(1)];
    %      X2=[a(2) b(2) c(2) d(2)];

    X1=Corner(:,1)';
    X2=Corner(:,2)';

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %  evalutes Function values at Gaussian points using affine mapping T   %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %for i=1:length(xi)
        %[x(i),y(i)] =T(xi(i),xi(i),X1,X2,ttype,type);
        %end
    for i=1:gauss_deg
        for j=1:gauss_deg
            f_v(i,j)=coeff(x_v(elem,i),y_v(elem,j),3);
            a_v(i,j)=coeff(x_v(elem,i),y_v(elem,j),1);
            b_v(i,j)=coeff(x_v(elem,i),y_v(elem,j),2);
            %dJ_v(i,j)=det_Jab(xi(i),xi(j),X1,X2,ttype,type);
            J=det_Jab(xi(i),xi(j),X1,X2);
            for k=1:2
                for l=1:2
                    if(J(k,l)~=0)
                        J1(k,l)=1/J(k,l);
                    else
                        J1(k,l)=0;
                    end
                end
            end
            J1=J1*J1';
            aa(i,j)=J1(1,1);
            bb(i,j)=J1(1,2);
            cc(i,j)=J1(2,2);
            dJ_v(i,j)=J(1,1)*J(2,2)-J(1,2)*J(2,1);
        end
    end
    
   
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %  construct Element mass and Element stiff matrix    %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
             for l = 1:gauss_deg
                 for k = 1:gauss_deg
                   ofi = 1;
                   for i1=1:(pol_deg+1)
                       for i2=1:(pol_deg+1)
                           if (i1+i2)<=(pol_deg+3)
                              EF(ofi)= EF(ofi)+w(l)*w(k)*f_v(l,k)* SF_v(i1,l)*SF_v(i2,k)*dJ_v(l,k);
                              ofj = 1;
                              for j1=1:(pol_deg+1)
                                  for j2=1:(pol_deg+1)
                                      if (j1+j2)<=(pol_deg+3)
                                         EA1(ofi,ofj)= EA1(ofi,ofj)+w(l)*w(k)*b_v(l,k)*SF_v(i1,l)*SF_v(i2,k)*SF_v(j1,l)*SF_v(j2,k)*dJ_v(l,k);
                                         EA(ofi,ofj)=EA(ofi,ofj)+w(l)*w(k)*a_v(l,k)*(aa(l,k)*dSF_dt_v(i1,l)*SF_v(i2,k)*dSF_dt_v(j1,l)*SF_v(j2,k)+bb(l,k)*(SF_v(i1,l)*dSF_dt_v(i2,k)*dSF_dt_v(j1,l)*SF_v(j2,k)+dSF_dt_v(i1,l)*SF_v(i2,k)*SF_v(j1,l)*dSF_dt_v(j2,k))+cc(i,j)*SF_v(i1,l)*dSF_dt_v(i2,k)*SF_v(j1,l)*dSF_dt_v(j2,k))*dJ_v(l,k);
                                         ofj=ofj+1;
                                      end
                                  end
                              end
                              ofi = ofi+1;
                           end
                       end
                   end
                 end
             end
             
             
     