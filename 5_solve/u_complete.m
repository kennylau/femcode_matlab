function u_c=u_complete(rci,B,u_0,rhs)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                                            %
% fucntion e_L2 =error_L21(num_of_elem,u_c,p,r,rci_full,rp,ri,gauss_deg,xi,w,ttype,type)                     % 
%                                                                                                            %
% Inputs:                                                                                                    %
%  * num_of_elem: number of element in the mesh                                                              %
%                                                                                                            %
%  *rci_full: which stores the global index value of interior nodes for each element K_j,1,...,num_of_elem   %
%             e.g the global index of the element 2 ,3 node =rci(ri(2)+3)                                    % 
%                                                                                                            %
%  * GS: Global Stiffness matrix                                                                             %
%                                                                                                            %
%  * GM: Global mass matrix                                                                                  %
%                                                                                                            %
%  * u_0: which store the constant value for interior golbal index,  1,...,max(rci_full)                     %
%                                                                                                            %
%  * rhs: which calculate f(v)-a(u_0,v), which is applying the Diriclet boundary condition                   %
%                                                                                                            %
% Outputs:                                                                                                   %
%                                                                                                            %
%  * u_c: which stores the constant value for each global index , 1,...,max(rci_full)                        %
%                                                                                                            %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

u_c=u_0;

u_c(1:max(rci))=B\rhs;
%B=full(B);

%[U,S,V] = svd(B,0);
%rhs1=U'*rhs;
%for i=1:length(rhs)
 %   rhs1(i)=rhs1(i)/S(i,i);
 %end

%u_c(1:max(rci))=V*rhs1;