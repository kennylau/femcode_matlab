function rhs=main_rhs(x_v,y_v,SF_v,dSF_dt_v,GL,num_of_elem,gauss_deg,p,t,rci,rci_full,u_0,rp,rd,xi,w,space)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                                            %
% fucntion rhs=main_rhs(SF_v,dSF_dt_v,GL,num_of_elem,gauss_deg,p,r,rci,rci_full,rcw,u_0,rp,ri,xi,w,type)     % 
%                                                                                                            %
% Inputs:                                                                                                    %
%  * SF_v:  which evaluates the SF for all the xi pts from pol_deg=1,...,max_pol_deg                         %
%                                                                                                            %
%  * dSF_dt_v:  which evaluates the dSF_dt for all the xi pts from pol_deg=1,...,max_pol_deg                 %
%                                                                                                            %
%  * GL: Global load vector                                                                                  %
%                                                                                                            %
%  * num_of_elem: number of element in the mesh                                                              %
%                                                                                                            %
%  * gauss_deg: order of Gaussian quadrature                                                                 %
%                                                                                                            %
%  * p: point matrix of size 2 x number of points                                                            %
%        row 1: x-coordinates of points                                                                      %
%        row 2: y-coordinates of points                                                                      %
%                                                                                                            %
%  * r: is the index of the corner vertex on element K_j,j=1,...,num_of_elem                                 %
%                                                                                                            %
%  *rci : which stores the global index value of interior nodes for each element K_j,1,...,num_of_elem       %
%             e.g the global index of the element 2 ,3 node =rci(ri(2)+3)                                    %
%                                                                                                            %
%  *rci_full: which stores the global index value of all nodes for each element K_j,1,...,num_of_elem        %
%             e.g the global index of the element 2 ,3 node =rci_full(ri(2)+3)                               %
%                                                                                                            %
%  *rcw: which stores the direction of the basis function for each element K_j,1,...,num_of_elem             %
%                                                                                                            %
%  * u_0: which store the constant value for interior golbal index,  1,...,max(rci_full)                     %
%                                                                                                            %
%  *rp: is a vector which stores the pol degree of each element K_j,j=1,...,num_of_elem                      %
%                                                                                                            %
%  *ri: which store the aggreciate dof of the elemnet                                                        %
%                                                                                                            %
%  * xi: Gaussian integration points                                                                         %
%                                                                                                            %
%  * w : Gaussian weight  points                                                                             %
%                                                                                                            %
%  *type (type of element) : triangle = 3                                                                    %
%                            quad     = 4                                                                    %
%                                                                                                            %
% Outputs:                                                                                                   %
%                                                                                                            %
%  * rhs: which calculate f(v)-a(u_0,v), which is applying the Diriclet boundary condition                      %
%                                                                                                            %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

rhs=GL;
for l = 1:num_of_elem

    for k = 1:4
        Corner(k,:) = p(:,t(k,l))';
    end
    
    [EA, EA1,EF] = FORM_EA_EF(x_v,y_v,l,Corner,SF_v,dSF_dt_v,rp(l),gauss_deg,xi,w,space);
     
    % check wheteher we have coeff of u_0 is non-zeros
    
    num_shape = rd(l+1)-rd(l);
    for i = 1:num_shape
        if (rci(rd(l)+i) > 0)
            for j = 1:num_shape
                if (rci_full(rd(l)+j) > 0)
                    rhs(rci(rd(l)+i)) = rhs(rci(rd(l)+i)) - u_0(rci_full(rd(l)+j))*(EA(j,i)+EA1(j,i));
                end
            end
        end
    end     
end