function J=det_Jab(eta,phi,X,Y)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                                            %
% fucntion J=det_Jab(eta,phi,X,Y,ttype,type)                                                                 % 
%                                                                                                            %
% Inputs:                                                                                                    %
%  * eta: a value beteween [-1,1], which \in reference element [-1,1]^2                                      % 
%                                                                                                            %
%  * phi: a value beteween [-1,1], which \in reference element [-1,1]^2                                      %
%                                                                                                            %
%  * X: which stores the x-coordinates of the element K_j , i.e.  [X1 X2 X3 X4]  for quad                    %
%                                                                                                            %
%  * Y: which stores the y-coordinates of the element K_j , i.e.  [Y1 Y2 Y3 Y4] for quad                     %
%                                                                                                            %
%  * ttype = mapping type i.e                                                                                %
%            (1)   linear,                                                                                   %
%            (2)   quadratic parametric mapping (need convensional shape function)                           %
%            (3) Blending function method (using in p_version generally)                                     %
%                                                                                                            %
%                                                                  
% Outputs:                                                                                                   %
%                                                                                                            %
%  which computes Jacobian matrix                                  
%                                                                                                            %
%                                                                  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


         
                  dx_eta = djsf(phi,X(3)-X(4),X(2)-X(1));
                  dx_phi = djsf(eta,X(3)-X(2),X(4)-X(1));
                  dy_eta = djsf(phi,Y(3)-Y(4),Y(2)-Y(1));
                  dy_phi = djsf(eta,Y(3)-Y(2),Y(4)-Y(1));
                  J=[ dx_eta  dx_phi;dy_eta dy_phi];
                  
                  %det_J=dx_eta*dy_phi-dx_phi*dy_eta;
            
  