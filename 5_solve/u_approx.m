function [u_h,duhdx,duhdy]= u_approx(eta,phi,J,elem,u_c,rci_full,rp,rd)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                                            %
% fucntion u_h= u_approx(x,y,num_of_elem,u_c,p,r,rci_full,rp,ri,type)                                        % 
%                                                                                                            %
% Inputs:                                                                                                    %
%  * x,y : a value \in K_j, j=1,..,num_of_elem                                                               % 
%                                                                                                            %
%  * num_of_elem: number of element in the mesh                                                              %
%                                                                                                            %
%  * u_c: which stores the constant value for each global index , 1,...,max(rci_full)                        %
%                                                                                                            %
%  * p: point matrix of size 2 x number of points                                                            %
%        row 1: x-coordinates of points                                                                      %
%        row 2: y-coordinates of points                                                                      %
%                                                                                                            %
%  * r: is the index of the corner vertex on element K_j,j=1,...,num_of_elem                                 %
%                                                                                                            %
%  *rci_full: which stores the global index value of all nodes for each element K_j,1,...,num_of_elem        %
%             e.g the global index of the element 2 ,3 node =rci_full(ri(2)+3)                               % 
%                                                                                                            %
%  *rp: is a vector which stores the pol degree of each element K_j,j=1,...,num_of_elem                      %
%                                                                                                            %
%  *ri: which store the aggreciate dof of the elemnet                                                        %
%                                                                                                            %
%  *type (type of element) : triangle = 3                                                                    %
%                            quad     = 4                                                                    %
%                                                                                                            %
% Outputs:                                                                                                   %
%                                                                                                            %
%  *u_h: which is the approx sol of the u on (x,y) \in K_j, j=1,..,num_of_elem                               %
%                                                                                                            %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



u_h=0;
duhdx=0;
duhdy=0;

for k=1:2
    for l=1:2
        if(J(k,l)~=0)
            J1(k,l)=1/J(k,l);
        else
            J1(k,l)=0;
        end
    end
end

   
of = 1;
for i=1:(rp(elem)+1)
    for j=1:(rp(elem)+1)
        if of<=basenum(rp(elem),2)
            if (rci_full(rd(elem)+of)>0) && (i+j)<=(rp(elem)+3)
                u_h = u_h+u_c(rci_full(rd(elem)+of))*SF(eta,i)*SF(phi,j);
                duhdx = duhdx+u_c(rci_full(rd(elem)+of))*(dSF_dt(eta,i)*SF(phi,j)*J1(1,1)+SF(eta,i)*dSF_dt(phi,j)*J1(1,2));
                duhdy = duhdy+u_c(rci_full(rd(elem)+of))*(dSF_dt(eta,i)*SF(phi,j)*J1(2,1)+SF(eta,i)*dSF_dt(phi,j)*J1(2,2));
                of = of+1;
            end
        end
    end 
end

    
       