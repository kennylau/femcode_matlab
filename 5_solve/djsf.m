function dx_eta = djsf(phi,C1,C2) 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                                            %
% fucntion dx_eta = djsf(phi,C1,C2)                                                                          % 
%                                                                                                            %
% Inputs:                                                                                                    %
%  * phi: a value beteween [-1,1], which \in reference element [-1,1]^2                                      %
%                                                                                                            %
%  *  C1: constant                                                                                           %
%                                                                                                            %
%  *  C2: constant                                                                                           %
%                                                                                                            %
% Outputs:                                                                                                   %
%                                                                                                            %
%  which computes dx/deta used on the Jacobian matrix of the transform type (ttype=1)                        %
%                                                                                                            %
%                                                                                                            %
% created by Yin Ching So (UNSW)                                                                             %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

dx_eta=(1/4)*((1+phi)*C1+(1-phi)*C2);