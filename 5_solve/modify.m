function [B_modify,rhs_modify]= modify(num_of_elem,p,r,rc,rci,B,rhs,bctype,kappa,rp,ri,SF_v,SF_v1,gauss_deg,xi,w,type)
 B_modify=B;
 
 
 rhs_modify=rhs;
 
if (bctype~=1)
                                          
    for l = 1:num_of_elem
   
   
   
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % constructs element stiffness matrix and mass matrix     %
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %Corner=[p(:,i1)';p(:,i2)';p(:,i3)';p(:,i4)'];
        for k=1:type
            Corner(k,:)=p(:,r(k,l))';
        end
        for k=1:type
            if(rc(k,l)==-2)
                
                [ER, ER1] = FORM_ER_ER1(kappa,Corner,SF_v,SF_v1,rp(l),gauss_deg,xi,w,1,k,type);
                
                num_shape=ri(l+1)-ri(l);
                for i=1:num_shape
                    for j=1:num_shape
                        if (rci(ri(l)+i)>0)&(rci(ri(l)+j)>0)&(ER(i,j)~=0)
                            B_modify(rci(ri(l)+i),rci(ri(l)+j))=B_modify(rci(ri(l)+i),rci(ri(l)+j))+ER(i,j);
                            
                        end
                        if (rci(ri(l)+j)>0)&(ER1(j)~=0)
                            rhs_modify(rci(ri(l)+j),1)=rhs_modify(rci(ri(l)+j),1)+ER1(j);%+rcw(ri(l)+j)*ER1(j);
          
                        end
                    end
                end
            end
        end
    end


end