function u_0 = u_zero(num_of_elem,p,t,rc,rp,rd,rci_full,space)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                                            
% fucntion u_0 = u_zero(num_of_elem,p,r,rc,rp,ri,rci_full,space,type)                                         
%                                                                                                            
% Inputs:                                                                                                    
%  * num_of_elem: number of element in the mesh                                                              
%                                                                                                            
%  * p: point matrix of size 2 x number of points                                                            
%        row 1: x-coordinates of points                                                                      
%        row 2: y-coordinates of points                                                                      
%                                                                                                            
%  * t: is the index of the corner vertex on element K_j,j=1,...,num_of_elem                                 
%                                                                                                            
%  *rc: is a matrix which stores the neighbour element index for each                                        
%      element K_j, j=1,...,num_of_elem                                                                      
%                                                                                                            
%  *rp: is a vector which stores the pol degree of each element K_j,j=1,...,num_of_elem                      
%                                                                                                            
%  *rd: which store the aggreciate dof of the elemnet                                                        
%                                                                                                            
%  *rci_full: which stores the global index value of all nodes for each element K_j,1,...,num_of_elem        
%             e.g the global index of the element 2 ,3 node =rci_full(ri(2)+3)                                
%                                                                                                            
%  * space: finite element subspace                                                                          
%           where,                                                                                           
%                 space 1 is Q_{p}^{'}                                                                       
%                 space 2 is Q_{p}^{''}                                                                      
%                 space 3 is Q_{p}                                                                           
%                                                                                                            
%  *meshtype (type of element) : triangle = 3                                                                
%                            quad     = 4                                                                    
%                                                                                                            
% Outputs:                                                                                                   
%                                                                                                            
%  * u_0: which store the constant value for boundary golbal index for Diriclet boundary condition           
%                                                                                                            
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

u_0=zeros(max(rci_full),1);
% vertex part 
for l=1:num_of_elem
    for k=1:4
        if(rc(k,l)==-1) % Diriclet bou_edg, then i compute the edg interpolation g (correspond to coeff(x,y,4)) on edg
            
            re = r_edge(k);
            %vertex part
            for i=1:2
                of = cei2loc(1,re(i),re(i),rp(l),space);
                u_0(rci_full(rd(l)+of))=coeff(p(1,t(re(i),l)),p(2,t(re(i),l)),4);
            end
            
            % bou edge part
            switch k
                case 1 % side 1
                    
                    r_hat_left=[ -1 -1];
                    r_hat_right=[1 -1];
                    Ixj=[3:(rp(l)+1);ones(1,(rp(l)-1))];
                   
                case 2

                    
                    r_hat_left=[1 -1];
                    r_hat_right=[1 1];
                    Ixj=[2*ones(1,(rp(l)-1));3:(rp(l)+1)];
                case 3
                    
                    r_hat_left=[1 1];
                    r_hat_right=[-1 1];
                    Ixj=[3:(rp(l)+1);2*ones(1,(rp(l)-1))];
                case 4
                   
                    r_hat_left=[-1 1];
                    r_hat_right=[-1 -1];
                    Ixj=[ones(1,(rp(l)-1));3:(rp(l)+1)];
                otherwise
                    error('boundary edge exceed')
           end  
           dx=1/rp(l);
       
           if (rp(l)>1)
                B=zeros((rp(l)-1),(rp(l)-1));
                l_b=zeros((rp(l)-1),1);
                for i=1:(rp(l)-1)     
                    t1=vf(r_hat_left,r_hat_right,i*dx);
                    eta=t1(1);
                    phi=t1(2);
                    [x,y] =T(eta,phi,p(1,t(:,l)),p(2,t(:,l)));
                    
                    Ix=[1 2 2 1;1 1 2 2];
                    l_b(i)=coeff(x,y,4);
                    for kk=1:2
                        of = cei2loc(1,re(kk),re(kk),rp(l),space);
                        l_b(i)=l_b(i)-u_0(rci_full(rd(l)+of))*SF(eta,Ix(1,re(kk)))*SF(phi,Ix(2,re(kk)));
                    end
                    
        
                    for j=1:(rp(l)-1)
                        B(i,j)=SF(eta,Ixj(1,j))*SF(phi,Ixj(2,j));
                        
                    end
                end
                %l
                %B
                %l_b
                c_l=B\l_b;
                for g=1:(rp(l)-1)
                     
                    of = cei2loc(2,k,g,rp(l),space);
                    u_0(rci_full(rd(l)+of))=c_l(g);
                end
                
           end
           
            
        end
    end
end
