function of = cei2loc(itype,ielem,inum,p,space) 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                             %
%      of = cei2loc(itype,ielem,inum,pol_deg,space,type)                      %
%                                                                             %
% Inputs:                                                                     %
%                                                                             %
%  * itype: 1 = corner                                                        %
%           2 = edge                                                          %
%           3 = interior                                                      %
%                                                                             %
%  * ielem  = when itype is 1, ielem is the corner num=1,...,4                %
%                         2, ielem is the edg num=1,...,4                     %
%                         3, ielem is the numbering of bubblem mode           %
%                                                                             %
% * inum= the numbering of the nodal pt on the edg, i.e. only make sense      %
%         when itype =2                                                       %
% * pol_deg: polynomial degree used in FEM                                    %
% * space: finite element subspace                                            %
%                                                                             %
%     where,                                                                  %
%      space 1 is Q_{p}^{'}                                                   %
%      space 2 is Q_{p}^{''}                                                  %
%      space 3 is Q_{p}                                                       %
% *type (type of element) : triangle = 3                                      %
%                           quad     = 4                                      %
%                                                                             %
% Outputs:                                                                    %
%   * n: number of dof in element K_j,j=1,...,num_of_elem according to the    %
%         space                                                               %
% created by Yin Ching So (UNSW)  
% modified by Yuxin SUN (UNSW)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    of=0;
    switch itype 
        case 1   % vertex
            Ix=[1 (p+2) (p+3) 2];
            if(ielem==inum)&(ielem<=length(Ix))
                of=Ix(ielem);
            end
        case 2  % boundary
            if (p>1)
                switch ielem % boundary 1
                    case 1
                        switch space
                            case 2
                                lib_Ix = [2*p+3,3*p+3,4*p+2,5*p,6*p-3,7*p-7,8*p-12];                               
                                Ix(1:p-1)=lib_Ix(1:p-1);
                            case 3
                                Ix=[(2*p+3):(p+1):(2*p+3)+(p-2)*(p+1)];
                        end
                    case 2  % boundary 2
                        Ix=[(p+4):2*(p+1)];
                    case 3  % boundary 3
                        switch space
                            case 2
                                lib_Ix = [2*p+4,3*p+4,4*p+3,5*p+1,6*p-2,7*p-6,8*p-11];
                                Ix(1:p-1)=lib_Ix(1:p-1);
                            case 3
                                Ix=[(2*(p+2)):(p+1):(2*(p+2))+(p-2)*(p+1)];
                        end
                    case 4  % boundary 4
                        Ix=[3:(p+1)];
                end
                if inum<=length(Ix)
                    of=Ix(inum);
                end
            end
        case 3  % inter 
            switch space
                case 3
                    num_int_pts_per_elem= basenum(p,space)-4*p;
                       if(num_int_pts_per_elem>0)
                         int_deg=ceil(sqrt(num_int_pts_per_elem));
       
                          count1=0;
                          lcount=int_deg;
                          count2=0;
                          for i=1:int_deg
           
           
                                 Ix(count2+[1:lcount])= (2*(p+2))+count1+[1:lcount];
                                 count2=count2+lcount;
                   
                                 fix1=0;
                  
                                 count1=count1+p+1;
                                 lcount=lcount+fix1;
                          end
                          if (inum<=length(Ix))&(inum==ielem)
                             of=Ix(inum);
                          end
                       end
                case 2
                   num_int_pts_per_elem= basenum(p,space)-4*p;
                   for lcount = 1:p-2
                       count1=(lcount-1)*p-0.5*lcount^2-0.5*lcount+2;
                       count2=lcount*p-0.5*lcount^2-1.5*lcount;
                       %remain_pts=num_int_pts_per_elem-count2;
                       in_count1=(lcount+1)*p-0.5*lcount^2+1.5*lcount+4;
                       in_count2=(lcount+2)*p-0.5*lcount^2+0.5*lcount+2;
                       Ix(count1:count2)=[in_count1:in_count2];
                   end
                   if inum<=length(Ix)
                    of=Ix(inum);
                   end
                       
                    
            end
        otherwise
            disp('invalid itype')
   
    end
   
end