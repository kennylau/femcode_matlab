function ind=vertex_dof(num_of_elem,num_of_node,t,rc,bctype)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                                            %
% fucntion  ind=vertex_dof(num_of_elem,num_of_node,r,rc,bctype,type)                                         % 
%                                                                                                            %
% Inputs:                                                                                                    %
%  * num_of_elem: number of element in the mesh                                                              %
%                                                                                                            %
%  * num_of_node: number of corner nodal pts in the mesh                                                     %
%                                                                                                            %
%  * t: is the index of the corner vertex on element K_j,j=1,...,num_of_elem                                 %
%                                                                                                            %
%  *rc: is a matrix which stores the neighbour element index for each                                        %
%      element K_j, j=1,...,num_of_elem                                                                      %
%                                                                                                            %
%                                                                    %
%                                                                                                            %
%  *meshtype (type of element) :                                                                %
%                            quad     = 4                                                                    %
%                                                                                                            %
% Outputs:                                                                                                   %
%                                                                                                            %
%  *ind: which stores the global index of the vertex nodes                                                   %
%        (this program is used for generate rci)                                                             %
%                                                                                                            %
% created by Yin Ching So (UNSW)      
% modified by Yuxin SUN
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


ind=zeros(num_of_node,1);
switch bctype
   
    case 1 % Dirichlet bc
        offset=1;
        for l=1:num_of_elem
       
            for j=1:4
                re = r_edge(j);
                
                
                if(((rc(re(1),l)==-1)||(rc(re(2),l)==-1)))&&(ind(t(re(2),l))==0)
                    ind(t(re(2),l))=-1;
                else
                    if(ind(t(re(2),l))==0)
                        ind(t(re(2),l))=offset;
                        offset=offset+1;
                    end
                    
                 
                end
            end
        end
                    
  
    otherwise
        disp('undefined type of boundary condition')
end
        
        