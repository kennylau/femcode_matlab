function [rci,rcw] = global_index(num_of_elem,num_of_node,t,rc,rp,space,bctype)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                                         %
% fucntion [rci,rcw] = global_index(num_of_elem,num_of_node,max_connection,r,rc,rp,space,bctype,type)     %
%                                                                                                         %
% Inputs:                                                                                                 %
%  * num_of_elem: number of element in the mesh                                                           %
%                                                                                                         %
%  * num_of_node: number of corner nodal pts in the mesh                                                  %
%                                                                                                         %
%  * max_connection: the maximum number of connection that can have on a                                  %
%                   single node                                                                           %
%                                                                                                         %
%  * r: is the index of the corner vertex on element K_j,j=1,...,num_of_elem                              %
%                                                                                                         %
%  *rc: is a matrix which stores the neighbour element index for each                                     %
%      element K_j, j=1,...,num_of_elem                                                                   %
%                                                                                                         %
%  *rp: is a vector which stores the pol degree of each element K_j,j=1,...,num_of_elem                   %
%                                                                                                         %
%  * space: finite element subspace                                                                       %
%           where,                                                                                        %
%                 space 1 is Q_{p}^{'}                                                                    %
%                 space 2 is Q_{p}^{''}                                                                   %
%                 space 3 is Q_{p}                                                                        %
%                                                                                                         %
%  * bctype: the boundary condition type                                                                  %
%                                                                                                         %
%  *type (type of element) :                                                                 %
%                            quad     = 4                                                                 %
%                                                                                                         %
% Outputs:                                                                                                %
%                                                                                                         %
%  *rci: which stores the global index value of the interior node for each element K_j,1,...,num_of_elem  %
%         e.g the global index of the element 2 ,3 node =rci(ri(2)+3)                                     % 
%                                                                                                         %
%  *rcw: which stores the direction of the basis function for each element K_j,1,...,num_of_elem          %
%                                                                                                         %
% created by Yin Ching So (UNSW)   
% modified by Yuxin SUN (UNSW)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%---------------------%
% initialise rci      %
%---------------------%
rd = r_dof(rp,space);
rci = zeros(rd(length(rd)),1);
rcw = ones(rd(length(rd)),1);

%---------------------%
%  vertex part        %
%---------------------%

ind=vertex_dof(num_of_elem,num_of_node,t,rc,bctype);
for l=1:num_of_elem
    for j=1:4
        ofi = cei2loc(1,j,j,rp(l),space);
        rci(rd(l)+ofi)=ind(t(j,l));
    end
end

%---------------------%
%  edge part          %
%---------------------%

offset=max(ind)+1;

    
        for l=1:num_of_elem
            for j=1:4
                if(rc(j,l)>0) % implies the edg of elem i is not on the bou
                    if (rc(j,l)>l) % we only apply on the edg where the elem we havnt touch yet
                 
                        num= numcei(2,j,rp(l),space); 
                        n=search_edg(j,l,rc,t);
                        num_n=numcei(2,n,rp(rc(j,l)),space);
                 
                    for k=1:min(num,num_n)
                   
                     
                        ofl = cei2loc(2,j,k,rp(l),space);
                        ofn = cei2loc(2,n,k,rp(rc(j,l)),space);
                        rci(rd(l)+ofl)=offset;
                        rci(rd(rc(j,l))+ofn)=offset;
                        if (t(r_edge(j),l)~=t(r_edge(n),rc(j,l)))
                            rcw(rd(rc(j,l))+ofn)=-1;
                        end
                        offset=offset+1;
                     end
                     end
                 end
            end
       end


        

%---------------------%
%  bubble part        %
%---------------------%
 
for l=1:num_of_elem
    int_deg=basenum(rp(l),space)-4*rp(l);
    for k=1:int_deg
        ofl = cei2loc(3,k,k,rp(l),space);
        rci(rd(l)+ofl)=offset;
        offset=offset+1;
    end
end