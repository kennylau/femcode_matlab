function rci_full= global_index_full(num_of_elem,num_of_node,t,rc,rp,rci,space,bctype)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                                            %
% fucntion rci_full= global_index_full(num_of_elem,num_of_node,max_connection,r,rc,rp,rci,space,bctype,type) % 
%                                                                                                            %
% Inputs:                                                                                                    %
%  * num_of_elem: number of element in the mesh                                                              %
%                                                                                                            %
%  * num_of_node: number of corner nodal pts in the mesh                                                     %
%                                                                                                            %
%  * max_connection: the maximum number of connection that can have on a                                     %
%                   single node                                                                              %
%                                                                                                            %
%  * r: is the index of the corner vertex on element K_j,j=1,...,num_of_elem                                 %
%                                                                                                            %
%  *rc: is a matrix which stores the neighbour element index for each                                        %
%      element K_j, j=1,...,num_of_elem                                                                      %
%                                                                                                            %
%  *rci: which stores the global index value of the interior node for each element K_j,1,...,num_of_elem     %
%         e.g the global index of the element 2 ,3 node =rci(ri(2)+3)                                        % 
%                                                                                                            %
%  *rp: is a vector which stores the pol degree of each element K_j,j=1,...,num_of_elem                      %
%                                                                                                            %
%  * space: finite element subspace                                                                          %
%           where,                                                                                           %
%                 space 1 is Q_{p}^{'}                                                                       %
%                 space 2 is Q_{p}^{''}                                                                      %
%                 space 3 is Q_{p}                                                                           %
%                                                                                                            %
%  * bctype: the boundary condition type                                                                     %
%                                                                                                            %
%  *type (type of element) : triangle = 3                                                                    %
%                            quad     = 4                                                                    %
%                                                                                                            %
% Outputs:                                                                                                   %
%                                                                                                            %
%  *rci_full: which stores the global index value of all nodes for each element K_j,1,...,num_of_elem        %
%             e.g the global index of the element 2 ,3 node =rci_full(ri(2)+3)                               % 
%                                                                                                            %
%  *rcw: which stores the direction of the basis function for each element K_j,1,...,num_of_elem             %
%                                                                                                            %
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
switch bctype
    case 1

    %---------------------%
    % initialise rci_full %
    %---------------------%

    rd= r_dof(rp,space);
    rci_full=rci;


    %---------------------%
    %  vertex part        %
    %---------------------%

    ind=vertex_dof(num_of_elem,num_of_node,t,rc,bctype);
    % no Boundary condition is apply means that i denote every vertex a golbal value index 
    % hence this part is used for the construction of rci_full
      
    if (max(rci)==-1)
        offset=1;
    else
        offset=max(rci)+1;
    end
    for i=1:length(ind)
        if(ind(i)<0)
            ind(i)=offset;
            offset=offset+1;
        end
    end
            
    for l=1:num_of_elem
        for j=1:4
            ofi = cei2loc(1,j,j,rp(l),space);
            rci_full(rd(l)+ofi)=ind(t(j,l));
        end
    end


    %---------------------%
    %  edge part          %
    %---------------------%

    for l=1:num_of_elem
        for j=1:4
            if(rc(j,l)<0) % implies the edg of elem i  on the bou
           
                 
                 num= numcei(2,j,rp(l),space); 
                 
                 
                 for k=1:num
                   
                     
                     ofl = cei2loc(2,j,k,rp(l),space);
                  
                     rci_full(rd(l)+ofl)=offset;
                        
                     
                     offset=offset+1;
                 end
             
            end
        end
    end

otherwise
        disp('undefined type of boundary condition')
end
        
        
                     
                     