function latex_table(p,N,e_H1,kappa,E_H1,lambda,Theta)
    i = 1;
    fprintf('\\multirow{%d}{*}{$%d$} & $%d$ & \\num{%.4e} & & \\num{%.4e} & & $%.4f$\\\\\n',length(N),p,N(i),e_H1(i),E_H1(i),Theta(i));
    for i = 2:length(N)
        fprintf('& $%d$ & \\num{%.4e} & $%.4f$ & \\num{%.4e} & $%.4f$ & $%.4f$\\\\\n',N(i),e_H1(i),kappa(i-1),E_H1(i),lambda(i-1),Theta(i));
    end
end