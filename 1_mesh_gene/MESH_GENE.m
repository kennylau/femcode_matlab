function [p,e,t] = MESH_GENE(a,b,c,d,Nx,Ny)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                             %
%[p,e,r] = MESH_GENE(a,b,c,d,n)                                             %
% defines a mesh on the //gram a,b,c,d \in R^2.  d  ____________  c           %
%                                                 /            /              %
% Inputs:                                        /____________/               %
%   * coordinates of corners: a,b,c,d          a              b              
%   * n: 2^n is the number of subintervals in each boundary
%   * N: number of subintervals in ||b-a||_2                                  %
%   * M: number of subintervals in ||d-a||_2                                  %
%                                                                             %
% Outputs:                                                                    %
%   * p: point matrix of size 2 x number of points                            %
%        row 1: x-coordinates of points                                       %
%        row 2: y-coordinates of points                                       %
%   * e: edge matrix of size  =   N*M                                         %
%                                                                             %
%        row 1: index of starting point of edge                               %
%        row 2: index of ending point of edge                                 %
%                                                                             %
%   * t: rectangle matrix of size =                                           %
%              dof on each element x number of elements                       %
%        row (1:4): index of verti,  i=1,...,4                                %
%        row (5:8): index of side i,                                          %
%        row (9:): index of internal mode pts
%                                                                             %
%                                                       side 3                %    
%                                                  4 ____________ 3           %
%                                                    |           |            %          
%                                            side 4  |           |  side 2    %
%                                                    |___________|            %
%                                                  1               2          %
%                                                       side 1                %              
%                                                                             %
%   * total_dof:   total degrees of freedom for space i,i=1,..,3              %
%                   =  num_ele_ver + num_sidemode_pts+num_int_pts             %
%                                                                             %
% modified by Yuxin SUN (UNSW)                                             %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
N=Nx;
M=Ny;


num_ele_ver = (M+1)*(N+1);               % number of element vertices
num_of_elem=N*M;                                           
num_bou_edg = 2*(M+N);                   % number of boundary edges


                                        
                                                                           
                                                               
dx = 1/N;
dy = 1/M;

p = zeros(2,num_ele_ver);
e = zeros(2,num_bou_edg);

t = zeros(4,M*N);

L_edg=vf(a,d,0:dy:1)';
R_edg=vf(b,c,0:dy:1)';
countrow=0;
for i=1:(M+1)
    p(:,(countrow+1):(countrow+N+1))=vf(L_edg(:,i)',R_edg(:,i)',0:dx:1)';
    countrow=countrow+N+1;
end
    
% basic e

%side1
e(1:2,1:N)=[1:N;2:(N+1)];
%side2
e(1:2,(N+1):(N+M))=[(N+1):(N+1):(num_ele_ver-(N+1));2*(N+1):(N+1):num_ele_ver];
%side3
e(1:2,(N+M+1):(2*N+M))=[num_ele_ver:-1:(num_ele_ver-N+1);(num_ele_ver-1):-1:(num_ele_ver-N)];
%side4
e(1:2,(2*N+M+1):num_bou_edg)=[(num_ele_ver-N):-(N+1):(N+2);(num_ele_ver-2*N-1):-(N+1):1];


% basic r
countnum=0;
countelem=0;
for i=1:M
    t(1:4,(countelem+1):(countelem+N))=countnum+[1:N;2:(N+1);(N+3):2*(N+1);(N+2):(2*(N+1)-1)];
    countnum=countnum+(N+1);
    countelem=countelem+N;
end
           
     

