function [x_v,y_v]=gen_pt(num_of_elem,p,t,xi)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% [x_v,y_v]=gen_pt(num_of_elem,p,t,xi,meshtype) is used to generate Gauss
% points matrix.
% 
%  Inputs:
%    *num_of_elem: the number of elements.
%    *p: point matrix of size 2 x number of points
%    *t:rectangle matrix of size =                                           
%              dof on each element x number of elements 
%    *xi:Gauss points
%    *meshtype: type of mesh:
%               triangle = 3
%               quadrilateral = 4
%
% Outputs:
%   * x_v: x-coordinates of Gauss points                                       %
%   * y_v: y-coordinates of Gauss points                                 
%   
% Modified by Yuxin SUN
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for l=1:num_of_elem
    % for k=1:type
     %   Corner(k,:)=p(:,r(k,l))';
     %end
     Corner(1:4,:)=p(:,t(1:4,l))';
    
    for i=1:length(xi)
        [x_v(l,i),y_v(l,i)] =T(xi(i),xi(i),Corner(:,1)',Corner(:,2)');
        
    end
end
