function alpha = ORDER(eh,h)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                      %
% function y = ORDER(eh,h)                                             %
% computes the order alpha in the convergence                          %
%     e_h = c h^{alpha}.                                               %
% For generality, h is a vector of maybe different component values.   %
% So  alpha = log(e_1/e_2) / log(h1/h2).                               %
%                                                                      %
% T. Tran 06/04/05                                                     %
%                                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
n = size(eh,1);
if n <= 1
   error('vector eh and h not of correct size')
end
eh = eh(:);
h = h(:);
alpha = zeros(n,1);
alpha = log(eh(1:end-1)./eh(2:end)) ./ log(h(1:end-1)./h(2:end));
