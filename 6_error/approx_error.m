function [E_n,dEndx,dEndy]= approx_error(gauss_deg,pol_deg,E_coeff,xi,X1,X2,meshtype)


    E_n = zeros(gauss_deg,gauss_deg);
    dEndx = zeros(gauss_deg,gauss_deg);
    dEndy = zeros(gauss_deg,gauss_deg);
    
    for i=1:gauss_deg
        for j=1:gauss_deg
            J=det_Jab(xi(i),xi(j),X1,X2);
            for k=1:2
                for l=1:2
                    if(J(k,l)~=0)
                        DJ(k,l)=1/J(k,l);
                    else
                        DJ(k,l)=0;
                    end
                end
            end
            
        end
    end
  
switch meshtype
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % for quadrilateral meshes    %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    case 4
        for q =1:gauss_deg
            for qq =1:gauss_deg
            
                of1=0;  
                of=1;

                if (mod(pol_deg,2))==1
                    for i = pol_deg-1:pol_deg
                        for j =pol_deg-1:pol_deg
                            if i~=pol_deg-1||j~=pol_deg-1
                                E_n(q,qq)=E_n(q,qq)+E_coeff(of1,1)*SF(xi(q),i)*SF(xi(qq),j);
                                dEndx(q,qq)= dEndx(q,qq)+E_coeff(of1,1)*(dSF_dt(xi(q),i)*SF(xi(qq),j)*DJ(1,1)+SF(xi(q),i)*dSF_dt(xi(qq),j)*DJ(1,2));
                                dEndy(q,qq)= dEndy(q,qq)+E_coeff(of1,1)*(dSF_dt(xi(q),i)*SF(xi(qq),j)*DJ(2,1)+SF(xi(q),i)*dSF_dt(xi(qq),j)*DJ(2,2));
                            end
                            of1=of1+1;
                        end
                    end
                else
                    for i = pol_deg
                        for j =pol_deg
                            E_n(q,qq)=E_n(q,qq)+E_coeff(of,1)*SF(xi(q),i)*SF(xi(qq),j);
                            dEndx(q,qq)= dEndx(q,qq)+E_coeff(of,1)*(dSF_dt(xi(q),i)*SF(xi(qq),j)*DJ(1,1)+SF(xi(q),i)*dSF_dt(xi(qq),j)*DJ(1,2));
                            dEndy(q,qq)= dEndy(q,qq)+E_coeff(of,1)*(dSF_dt(xi(q),i)*SF(xi(qq),j)*DJ(2,1)+SF(xi(q),i)*dSF_dt(xi(qq),j)*DJ(2,2));
                        end
                    end
                end
            end            
        end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % for rectangular  meshes     %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
    case 5
        for q =1:gauss_deg
            for qq =1:gauss_deg
                E_n(q,qq) = E_n(q,qq) + E_coeff(1)*SF(xi(q),pol_deg+2);
                dEndx(q,qq) = dEndx(q,qq) + E_coeff(1)*dSF_dt(xi(q),pol_deg+2)*DJ(1,1);
                dEndy(q,qq) = dEndy(q,qq) + E_coeff(1)*dSF_dt(xi(q),pol_deg+2)*DJ(2,1);

                E_n(q,qq) = E_n(q,qq) + E_coeff(2)*SF(xi(qq),pol_deg+2);
                dEndx(q,qq) = dEndx(q,qq) + E_coeff(2)*dSF_dt(xi(qq),pol_deg+2)*DJ(1,2);
                dEndy(q,qq) = dEndy(q,qq) + E_coeff(2)*dSF_dt(xi(qq),pol_deg+2)*DJ(2,2);
            end
        end
end
    
