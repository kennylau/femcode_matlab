function [en_L2,en_H1,un_L2,un_H1,uapproxn_L2,uapproxn_H1,xplot,yplot,u_plot,du_dx_plot,du_dy_plot,uapprox_plot,du_approxdx_plot,du_approxdy_plot] =error_L2H1(Nx,Ny,x_v,y_v,u_c,p,t,rci_full,rp,rd,gauss_deg,xi,w)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                                            %
% fucntion e_L2 =error_L21(num_of_elem,u_c,p,r,rci_full,rp,ri,gauss_deg,xi,w,type)                     % 
%                                                                                                            %
% Inputs:                                                                                                    %
%  * num_of_elem: number of element in the mesh                                                              %
%                                                                                                            %
%  * u_c: which stores the constant value for each global index , 1,...,max(rci_full)                        %
%                                                                                                            %
%  * p: point matrix of size 2 x number of points                                                            %
%        row 1: x-coordinates of points                                                                      %
%        row 2: y-coordinates of points                                                                      %
%                                                                                                            %
%  * r: is the index of the corner vertex on element K_j,j=1,...,num_of_elem                                 %
%                                                                                                            %
%  *rci_full: which stores the global index value of all nodes for each element K_j,1,...,num_of_elem        %
%             e.g the global index of the element 2 ,3 node =rci_full(ri(2)+3)                               % 
%                                                                                                            %
%  *rp: is a vector which stores the pol degree of each element K_j,j=1,...,num_of_elem                      %
%                                                                                                            %
%  *ri: which store the aggreciate dof of the elemnet                                                        %
%                                                                                                            %
%  * gauss_deg: order of Gaussian quadrature                                                                 %
%                                                                                                            %
%  * xi: Gaussian integration points                                                                         %
%                                                                                                            %
%  * w : Gaussian weight  points                                                                             %
%                                                                                                            %
%                                                                                                            %
%                                                                                                            %
% Outputs:                                                                                                   %
%                                                                                                            %
%  *e_L2 : which calculate the error L_2 norm.
%  *e_H1 : which calculate the error H_1 norm                                                                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



num_of_elem = Nx*Ny;
en_L2 = zeros(num_of_elem,1);
en_H1 = zeros(num_of_elem,1);
un_L2 = zeros(num_of_elem,1);
un_H1 = zeros(num_of_elem,1);
uapproxn_L2 = zeros(num_of_elem,1);
uapproxn_H1 = zeros(num_of_elem,1);

xplot = reshape(x_v(1:Nx,end:-1:1)',[1 Nx*gauss_deg]);
yplot = reshape(y_v(1:Nx:num_of_elem,end:-1:1)',[1 Ny*gauss_deg]);
uapprox_plot = zeros(Nx*gauss_deg,Ny*gauss_deg);
u_plot = zeros(Nx*gauss_deg,Ny*gauss_deg);

for l=1:num_of_elem
    
    Corner(1:4,:)=p(:,t(1:4,l))';
    
    dJ_v = zeros(gauss_deg);
    u_approx_v = zeros(gauss_deg);
    du_approxdx = zeros(gauss_deg);
    du_approxdy = zeros(gauss_deg);
    u_v = zeros(gauss_deg);
    du_vdx = zeros(gauss_deg);
    du_vdy = zeros(gauss_deg);
    
    for q=1:gauss_deg
        for qq=1:gauss_deg
            J=det_Jab(xi(q),xi(qq),Corner(:,1)',Corner(:,2)');
            dJ_v(q,qq)=J(1,1)*J(2,2)-J(1,2)*J(2,1);
            [u_approx_v(q,qq),du_approxdx(q,qq),du_approxdy(q,qq)]= u_approx(xi(q),xi(qq),J,l,u_c,rci_full,rp,rd);
            %u_exact,dudx_exact,dudy_exact
            u_v(q,qq)=coeff(x_v(l,q),y_v(l,qq),4);
            du_vdx(q,qq)=coeff(x_v(l,q),y_v(l,qq),7);
            du_vdy(q,qq)=coeff(x_v(l,q),y_v(l,qq),8);
        end
    end
    
    xst = mod((l-1),Nx)*gauss_deg + 1;
    xend = xst+gauss_deg-1;
    yst = floor((l-1)/Nx)*gauss_deg+1;
    yend = yst+gauss_deg-1;
    uapprox_plot(xst:xend,yst:yend) = u_approx_v(end:-1:1,end:-1:1);
    du_approxdx_plot(xst:xend,yst:yend) = du_approxdx(end:-1:1,end:-1:1);
    du_approxdy_plot(xst:xend,yst:yend) = du_approxdy(end:-1:1,end:-1:1);
    u_plot(xst:xend,yst:yend) = u_v(end:-1:1,end:-1:1);
    du_dx_plot(xst:xend,yst:yend) = du_vdx(end:-1:1,end:-1:1);
    du_dy_plot(xst:xend,yst:yend) = du_vdy(end:-1:1,end:-1:1);
    
    for b=1:gauss_deg
        for k=1:gauss_deg
            %J=det_Jab(xi(b),xi(k),Corner(:,1)',Corner(:,2)',ttype,type);
            %dJ=J(1,1)*J(2,2)-J(1,2)*J(2,1);
            un_L2(l) = un_L2(l) + w(b)*w(k)*u_v(b,k)^2*dJ_v(b,k);
            un_H1(l) = un_H1(l) + w(b)*w(k)*(du_vdx(b,k)^2 + du_vdy(b,k)^2)*dJ_v(b,k) + w(b)*w(k)*u_v(b,k)^2*dJ_v(b,k);
            uapproxn_L2(l) = uapproxn_L2(l) + w(b)*w(k)*u_approx_v(b,k)^2*dJ_v(b,k);
            uapproxn_H1(l) = uapproxn_H1(l) + w(b)*w(k)*(du_approxdx(b,k)^2 + du_approxdy(b,k)^2)*dJ_v(b,k) + w(b)*w(k)*u_approx_v(b,k)^2*dJ_v(b,k);
            en_L2(l) = en_L2(l) + w(b)*w(k)*(u_approx_v(b,k) - u_v(b,k))^2*dJ_v(b,k);
            en_H1(l) = en_H1(l) + w(b)*w(k)*((du_approxdx(b,k) - du_vdx(b,k))^2 + (du_approxdy(b,k) - du_vdy(b,k))^2)*dJ_v(b,k) + w(b)*w(k)*(u_approx_v(b,k) - u_v(b,k))^2*dJ_v(b,k);
        end
    end
end

u_plot = u_plot';
uapprox_plot = uapprox_plot';

en_L2=sqrt(en_L2);
en_H1=sqrt(en_H1);