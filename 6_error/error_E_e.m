function[En_L2,En_H1,E_plot,dE_dx_plot,dE_dy_plot]= error_E_e(dom,Nx,Ny,p,t,x_v,y_v,gauss_deg,pol_deg,SF_v,dSF_dt_v,xi,w,meshtype,rci_full,rp,rd,u_c)

num_of_elem = Nx*Ny;
En_L2 = zeros(num_of_elem,1);
En_H1 = zeros(num_of_elem,1);

E_plot = zeros(Nx*gauss_deg,Ny*gauss_deg);

for n = 1:num_of_elem
    for k=1:4
        Corner(k,:)=p(:,t(k,n))';
    end
    X1=Corner(:,1)';
    X2=Corner(:,2)';
      
    [EA,EA1,EF,EF1,EF2,EF3] = A_FORM_EA_EF(dom,Nx,Ny,gauss_deg,x_v,y_v,xi,w,X1,X2,pol_deg,meshtype,SF_v,dSF_dt_v,n,rci_full,rp,rd,u_c);
    
    E_coeff = (EA+EA1)\(EF-EF1-EF2+EF3);
  
    [E_n,dEndx,dEndy]= approx_error(gauss_deg,pol_deg,E_coeff,xi,X1,X2,meshtype);
    
    xst = mod((n-1),Nx)*gauss_deg + 1;
    xend = xst+gauss_deg-1;
    yst = floor((n-1)/Nx)*gauss_deg+1;
    yend = yst+gauss_deg-1;
    E_plot(xst:xend,yst:yend) = E_n(end:-1:1,end:-1:1);
    dE_dx_plot(xst:xend,yst:yend) = dEndx(end:-1:1,end:-1:1);
    dE_dy_plot(xst:xend,yst:yend) = dEndy(end:-1:1,end:-1:1);
 
    for g1=1:gauss_deg
        for g2=1:gauss_deg
            J=det_Jab(xi(g1),xi(g2),X1,X2);
            dJ_v(g1,g2)=J(1,1)*J(2,2)-J(1,2)*J(2,1);

            En_L2(n) = En_L2(n) + w(g1)*w(g2)*(E_n(g1,g2))^2*dJ_v(g1,g2);
            En_H1(n) = En_H1(n) + w(g1)*w(g2)*(dEndx(g1,g2)^2+dEndy(g1,g2)^2)*dJ_v(g1,g2) +w(g1)*w(g2)*(E_n(g1,g2))^2*dJ_v(g1,g2);
        end
    end
end

E_plot = E_plot';

En_L2 = sqrt(En_L2);
En_H1 = sqrt(En_H1);