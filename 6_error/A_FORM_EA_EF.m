function [EA,EA1,EF,EF1,EF2,EF3] = A_FORM_EA_EF(dom,Nx,Ny,gauss_deg,x_v,y_v,xi,w,X1,X2,pol_deg,meshtype,SF_v,dSF_dt_v,elem,rci_full,rp,rd,u_c)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% function [EA,EA1,EF,EF1,EF2] = A_FORM_EA_EF(gauss_deg,x_v,y_v,xi,w,X1,X2,pol_deg,
% meshtype,SF_v,dSF_dt_v,elem,rci_full,rp,rd,u_c)
% 
% Inputs: 
%
%  * gauss_deg: order of Gaussian quadrature
%
%   * x_v: x-coordinates of Gauss points    
%
%   * y_v: y-coordinates of Gauss points  
%  
%  * xi: Gaussian integration points                                                                         
%                                                                                                            
%  * w : Gaussian weight  points     
%
%  * pol_deg: the maximum number of pol_deg that can have on element K_j,j=1,...,num_of_elem 
%
%  * meshtype (type of element) : rect = 5                                                                    
%                                 quad = 4   
%
%  * SF_v:  which evaluates the SF for all the xi pts from pol_deg=1,...,max_pol_deg                      
%                                                                                                         
%  * dSF_dt_v:  which evaluates the dSF_dt for all the xi pts from pol_deg=1,...,max_pol_deg    
%
%  *rci_full: which stores the global index value of all nodes for each element K_j,1,...,num_of_elem        
%             e.g the global index of the element 2 ,3 node =rci_full(ri(2)+3)                               
%                                                                                                                                   
%  *rp: is a vector which stores the pol degree of each element K_j,j=1,...,num_of_elem                   
%                                                                                                         
%  *rd: which store the aggreciate dof of the elemnet                                                     
%       hence , # of local dof of element i = rd(i+1)-rd(i);                                              
%                                                                
%  * u_c: which stores the constant value for each global index , 1,...,max(rci_full) 
%
%
%
%
%
%


switch meshtype
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % for quadrilateral meshes %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%
    case 4
        if (mod(pol_deg,2))==1 %% if p is odd numbers
            EF  = zeros(3,1);
            EF1 = zeros(3,1);
            EF2 = zeros(3,1);
            EA  = zeros(3,3);
            EA1 = zeros(3,3);
            for m = 1:gauss_deg
                for s = 1:gauss_deg
                    f(m,s)=coeff(x_v(elem,m),y_v(elem,s),3);
                    a(m,s)=coeff(x_v(elem,m),y_v(elem,s),1);
                    b(m,s)=coeff(x_v(elem,m),y_v(elem,s),2);
                    J=det_Jab(xi(m),xi(s),X1,X2);
                    for count1=1:2
                        for count2=1:2
                            if (J(count1,count2)~=0)
                                DJ(count1,count2)=1/J(count1,count2);
                            else
                                DJ(count1,count2)=0;
                            end
                        end
                    end
                    DJ_11(m,s)=DJ(1,1);
                    DJ_12(m,s)=DJ(1,2);
                    DJ_22(m,s)=DJ(2,2);
                    
                    J11=DJ*DJ';
                    J11_11(m,s)=J11(1,1);
                    J11_12(m,s)=J11(1,2);
                    J11_22(m,s)=J11(2,2);
                    
                    dJ(m,s)= J(1,1)*J(2,2)-J(1,2)*J(2,1);
                    
              
                    ofk=1;
                    for k1 = 1:(rp(elem)+1);
                        for k2 = 1:(rp(elem)+1);
                            if ofk<=basenum(rp(elem),2)
                                 if (rci_full(rd(elem)+ofk)>0) && (k1+k2)<=(rp(elem)+3)
                                     
                            ofj=1;
                            for j1=pol_deg-1:pol_deg;
                                for j2=pol_deg-1:pol_deg;
                                    if j1~=pol_deg-1||j2~=pol_deg-1
                                        EF(ofj)= EF(ofj)+ w(m)*w(s)*f(m,s)* SF_v(j1,m)*SF_v(j2,s)*dJ(m,s);
                                        EF1(ofj)= EF1(ofj)+ w(m)*w(s)*u_c(rci_full(rd(elem)+ofk))*a(m,s)*(J11_11(m,s)*dSF_dt_v(j1,m)*SF_v(j2,s)*dSF_dt_v(k1,m)*SF_v(k2,s)+J11_12(m,s)*(SF_v(j1,m)*dSF_dt_v(j2,s)*dSF_dt_v(k1,m)*SF_v(k2,s)+dSF_dt_v(j1,m)*SF_v(j2,s)*SF_v(k1,m)*dSF_dt_v(k2,s))+J11_22(m,s)*SF_v(j1,m)*dSF_dt_v(j2,s)*SF_v(k1,m)*dSF_dt_v(k2,s))*dJ(m,s);
                                        EF2(ofj)= EF2(ofj)+ w(m)*w(s)*u_c(rci_full(rd(elem)+ofk))*b(m,s)*SF_v(k1,m)*SF_v(k2,s)*SF_v(j1,m)*SF_v(j2,s)*dJ(m,s);
                                        
                                        ofi=1;
                                        for i1=pol_deg-1:pol_deg;
                                            for i2=pol_deg-1:pol_deg
                                                if i1~=pol_deg-1||i2~=pol_deg-1;
                                                    EA1(ofj,ofi)= EA1(ofj,ofi)+w(m)*w(s)*b(m,s)*SF_v(j1,m)*SF_v(j2,s)*SF_v(i1,m)*SF_v(i2,s)*dJ(m,s);
                                                    EA(ofj,ofi)=EA(ofj,ofi)+w(m)*w(s)*a(m,s)*(J11_11(m,s)*dSF_dt_v(j1,m)*SF_v(j2,s)*dSF_dt_v(i1,m)*SF_v(i2,s)+J11_12(m,s)*(SF_v(j1,m)*dSF_dt_v(j2,s)*dSF_dt_v(i1,m)*SF_v(i2,s)+dSF_dt_v(j1,m)*SF_v(j2,s)*SF_v(i1,m)*dSF_dt_v(i2,s))+J11_22(m,s)*SF_v(j1,m)*dSF_dt_v(j2,s)*SF_v(i1,m)*dSF_dt_v(i2,s))*dJ(m,s);
                                           
                                                    ofi=ofi+1;
                                                end
                                            end
                                        end
                                       
                                        ofj=ofj+1;
                                    end
                                end
                            end
                            
                             ofk = ofk+1; 
                                 end
                            end
                          
                        end
                    end
                    
                    
                    
                end
            end
            
            
        else  %% if p is even numbers
             EF  = zeros(1,1);
             EF1 = zeros(1,1);
             EF2 = zeros(1,1);
             EA  = zeros(1,1);
             EA1 = zeros(1,1);
             
             
             for m = 1:gauss_deg
                for s = 1:gauss_deg
                    f(m,s)=coeff(x_v(elem,m),y_v(elem,s),3);
                    a(m,s)=coeff(x_v(elem,m),y_v(elem,s),1);
                    b(m,s)=coeff(x_v(elem,m),y_v(elem,s),2);
                    J=det_Jab(xi(m),xi(s),X1,X2);
                    for count1=1:2
                        for count2=1:2
                            if (J(count1,count2)~=0)
                                DJ(count1,count2)=1/J(count1,count2);
                            else
                                DJ(count1,count2)=0;
                            end
                        end
                    end
                    DJ_11(m,s)=DJ(1,1);
                    DJ_12(m,s)=DJ(1,2);
                    DJ_22(m,s)=DJ(2,2);
                    
                    J11=DJ*DJ';
                    J11_11(m,s)=J11(1,1);
                    J11_12(m,s)=J11(1,2);
                    J11_22(m,s)=J11(2,2);
                    
                    dJ(m,s)= J(1,1)*J(2,2)-J(1,2)*J(2,1);
                    
              
                    ofk=1;
                    for k1 = 1:(rp(elem)+1);
                        for k2 = 1:(rp(elem)+1);
                            if ofk<=basenum(rp(elem),2)
                                if (rci_full(rd(elem)+ofk)>0) && (k1+k2)<=(rp(elem)+3)
                                    
                            ofj=1;
                            for j1=pol_deg
                                for j2=pol_deg
                                    
                                        EF(ofj)= EF(ofj)+ w(m)*w(s)*f(m,s)* SF_v(j1,m)*SF_v(j2,s)*dJ(m,s);
                                        EF1(ofj)= EF1(ofj)+ w(m)*w(s)*u_c(rci_full(rd(elem)+ofk))*a(m,s)*(J11_11(m,s)*dSF_dt_v(j1,m)*SF_v(j2,s)*dSF_dt_v(k1,m)*SF_v(k2,s)+J11_12(m,s)*(SF_v(j1,m)*dSF_dt_v(j2,s)*dSF_dt_v(k1,m)*SF_v(k2,s)+dSF_dt_v(j1,m)*SF_v(j2,s)*SF_v(k1,m)*dSF_dt_v(k2,s))+J11_22(m,s)*SF_v(j1,m)*dSF_dt_v(j2,s)*SF_v(k1,m)*dSF_dt_v(k2,s))*dJ(m,s);
                                        EF2(ofj)= EF2(ofj)+ w(m)*w(s)*u_c(rci_full(rd(elem)+ofk))*b(m,s)*SF_v(k1,m)*SF_v(k2,s)*SF_v(j1,m)*SF_v(j2,s)*dJ(m,s);
                                        
                                        ofi=1;
                                        for i1=pol_deg
                                            for i2=pol_deg
                                                
                                                    EA1(ofj,ofi)= EA1(ofj,ofi)+w(m)*w(s)*b(m,s)*SF_v(j1,m)*SF_v(j2,s)*SF_v(i1,m)*SF_v(i2,s)*dJ(m,s);
                                                    EA(ofj,ofi)=EA(ofj,ofi)+w(m)*w(s)*a(m,s)*(J11_11(m,s)*dSF_dt_v(j1,m)*SF_v(j2,s)*dSF_dt_v(i1,m)*SF_v(i2,s)+J11_12(m,s)*(SF_v(j1,m)*dSF_dt_v(j2,s)*dSF_dt_v(i1,m)*SF_v(i2,s)+dSF_dt_v(j1,m)*SF_v(j2,s)*SF_v(i1,m)*dSF_dt_v(i2,s))+J11_22(m,s)*SF_v(j1,m)*dSF_dt_v(j2,s)*SF_v(i1,m)*dSF_dt_v(i2,s))*dJ(m,s);
                                           
                                                    ofi=ofi+1;
                                               
                                            end
                                        end
                                       
                                        ofj=ofj+1;
                                    
                                end
                            end
                             ofk = ofk+1;
                                end
                            end
                            
                           
                        end
                    end
                    
                    
                    
                end
            end
        
        end
            
             
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % for rectangular  meshes     %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
    case 5
        EF  = zeros(2,1);
        EF1 = zeros(2,1);
        EF2 = zeros(2,1);
        EF3 = zeros(2,1);
        EA  = zeros(2,2);
        EA1 = zeros(2,2);
        EA2 = zeros(2,2);

        p_1=pol_deg+1;
        p_2=pol_deg+2;
        p_3=pol_deg+3;
        
        if elem == 1
            %for i = 1:size(x_v,1)
            %    disp(i);
            %    disp(x_v(i,:));
            %    disp(y_v(i,:));
            %end
        end
        
        for m = 1:gauss_deg
            for s = 1:gauss_deg
                f = coeff(x_v(elem,m),y_v(elem,s),3);
                a = coeff(x_v(elem,m),y_v(elem,s),1);
                b = coeff(x_v(elem,m),y_v(elem,s),2);
                J = det_Jab(xi(m),xi(s),X1,X2);
                for count1=1:2
                    for count2=1:2
                        if (J(count1,count2)~=0)
                            DJ(count1,count2)=1/J(count1,count2);
                        else
                            DJ(count1,count2)=0;
                        end
                    end
                end

                J11=DJ*DJ';
                dJ = J(1,1)*J(2,2) - J(1,2)*J(2,1);

                EA(1,1) = EA(1,1) + w(m)*w(s)*a*J11(1,1)*dSF_dt_v(p_2,m)*dSF_dt_v(p_2,m)*dJ;
                EA(1,2) = EA(1,2) + w(m)*w(s)*a*J11(1,2)*dSF_dt_v(p_2,m)*dSF_dt_v(p_2,s)*dJ;
                EA(2,1) = EA(2,1) + w(m)*w(s)*a*J11(2,1)*dSF_dt_v(p_2,s)*dSF_dt_v(p_2,m)*dJ;
                EA(2,2) = EA(2,2) + w(m)*w(s)*a*J11(2,2)*dSF_dt_v(p_2,s)*dSF_dt_v(p_2,s)*dJ;
                
                EA1(1,1) = EA1(1,1) + w(m)*w(s)*(b+1)*SF_v(p_2,m)*SF_v(p_2,m)*dJ;
                EA1(1,2) = EA1(1,2) + w(m)*w(s)*(b+1)*SF_v(p_2,m)*SF_v(p_2,s)*dJ;
                EA1(2,1) = EA1(2,1) + w(m)*w(s)*(b+1)*SF_v(p_2,s)*SF_v(p_2,m)*dJ;
                EA1(2,2) = EA1(2,2) + w(m)*w(s)*(b+1)*SF_v(p_2,s)*SF_v(p_2,s)*dJ;

                EF(1)= EF(1)+ w(m)*w(s)*f*SF_v(p_2,m)*dJ;
                EF(2)= EF(2)+ w(m)*w(s)*f*SF_v(p_2,s)*dJ;

                ofk=1;
                for k1 = 1:(rp(elem)+1)
                    for k2 = 1:(rp(elem)+1)
                        if k1+k2 <= rp(elem)+3
                            %if elem == 1 && u_c(rci_full(rd(elem)+ofk)) > 0
                                %disp([elem xi(m) xi(s) k1-1 k2-1 w(m)*w(s)*J11(1,1)*dJ u_c(rci_full(rd(elem)+ofk)) dSF_dt_v(p_2,m) dSF_dt_v(k1,m) SF_v(k2,s) w(m)*w(s)*u_c(rci_full(rd(elem)+ofk))*J11(1,1)*dSF_dt_v(p_2,m)*dSF_dt_v(k1,m)*SF_v(k2,s)*dJ]);
                            %end
                            EF1(1)= EF1(1) + w(m)*w(s)*u_c(rci_full(rd(elem)+ofk))*a*(J11(1,1)*dSF_dt_v(p_2,m)*dSF_dt_v(k1,m)*SF_v(k2,s)+J11(1,2)*dSF_dt_v(p_2,m)*SF_v(k1,m)*dSF_dt_v(k2,s))*dJ;
                            EF1(2)= EF1(2) + w(m)*w(s)*u_c(rci_full(rd(elem)+ofk))*a*(J11(2,1)*dSF_dt_v(p_2,s)*dSF_dt_v(k1,m)*SF_v(k2,s)+J11(2,2)*dSF_dt_v(p_2,s)*SF_v(k1,m)*dSF_dt_v(k2,s))*dJ;

                            EF2(1)= EF2(1) + w(m)*w(s)*u_c(rci_full(rd(elem)+ofk))*b*SF_v(k1,m)*SF_v(k2,s)*SF_v(p_2,m)*dJ;
                            EF2(2)= EF2(2) + w(m)*w(s)*u_c(rci_full(rd(elem)+ofk))*b*SF_v(k1,m)*SF_v(k2,s)*SF_v(p_2,s)*dJ;
                            ofk = ofk + 1;
                        end
                    end
                end
            end
        end
        
        % Boundary integrals
        for m = 1:gauss_deg
            al = coeff(X1(1),y_v(elem,m),1);
            ar = coeff(X1(2),y_v(elem,m),1);
            ad = coeff(x_v(elem,m),X2(1),1);
            au = coeff(x_v(elem,m),X2(4),1);
            
            ofk = 1;
            for k1 = 1:(rp(elem)+1)
                for k2 = 1:(rp(elem)+1)
                    if k1+k2 <= rp(elem)+3
                        % bottom edge
                        if X2(1) == dom(3)
                            EF3(1) = EF3(1) + w(m)*u_c(rci_full(rd(elem)+ofk))*SF_v(p_2,m)*DJ(2,2)*SF_v(k1,m)*(-ad*dSF_dt(-1,k2))*J(1,1);
                        else
                            EF3(1) = EF3(1) + 1/2*w(m)*u_c(rci_full(rd(elem)+ofk))*SF_v(p_2,m)*DJ(2,2)*SF_v(k1,m)*(-ad*dSF_dt(-1,k2))*J(1,1);
                            EF3(1) = EF3(1) - 1/2*w(m)*u_c(rci_full(rd(elem-Nx)+ofk))*SF_v(p_2,m)*DJ(2,2)*SF_v(k1,m)*ad*dSF_dt(1,k2)*J(1,1);
                        end
                        
                        % top edge
                        if X2(4) == dom(4)
                            EF3(1) = EF3(1) + w(m)*u_c(rci_full(rd(elem)+ofk))*SF_v(p_2,m)*DJ(2,2)*SF_v(k1,m)*au*dSF_dt(1,k2)*J(1,1);
                        else
                            EF3(1) = EF3(1) + 1/2*w(m)*u_c(rci_full(rd(elem)+ofk))*SF_v(p_2,m)*DJ(2,2)*SF_v(k1,m)*au*dSF_dt(1,k2)*J(1,1);
                            EF3(1) = EF3(1) - 1/2*w(m)*u_c(rci_full(rd(elem+Nx)+ofk))*SF_v(p_2,m)*DJ(2,2)*SF_v(k1,m)*(-au*dSF_dt(-1,k2))*J(1,1);
                        end
                        
                        % left edge
                        if X1(1) == dom(1)
                            EF3(2) = EF3(2) + w(m)*u_c(rci_full(rd(elem)+ofk))*SF_v(p_2,m)*DJ(1,1)*(-al*dSF_dt(-1,k1))*SF_v(k2,m)*J(2,2);
                        else
                            EF3(2) = EF3(2) + 1/2*w(m)*u_c(rci_full(rd(elem)+ofk))*SF_v(p_2,m)*DJ(1,1)*(-al*dSF_dt(-1,k1))*SF_v(k2,m)*J(2,2);
                            EF3(2) = EF3(2) - 1/2*w(m)*u_c(rci_full(rd(elem-1)+ofk))*SF_v(p_2,m)*DJ(1,1)*al*dSF_dt(1,k1)*SF_v(k2,m)*J(2,2);
                        end
                        
                        % right edge
                        if X1(2) == dom(2)
                            EF3(2) = EF3(2) + w(m)*u_c(rci_full(rd(elem)+ofk))*SF_v(p_2,m)*DJ(1,1)*ar*dSF_dt(1,k1)*SF_v(k2,m)*J(2,2);
                        else
                            EF3(2) = EF3(2) + 1/2*w(m)*u_c(rci_full(rd(elem)+ofk))*SF_v(p_2,m)*DJ(1,1)*ar*dSF_dt(1,k1)*SF_v(k2,m)*J(2,2);
                            EF3(2) = EF3(2) - 1/2*w(m)*u_c(rci_full(rd(elem+1)+ofk))*SF_v(p_2,m)*DJ(1,1)*(-ar*dSF_dt(-1,k1))*SF_v(k2,m)*J(2,2);
                        end
                        ofk = ofk + 1;
                    end
                end
            end
        end
    end
end