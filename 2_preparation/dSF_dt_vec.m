function dSF_dt_v = dSF_dt_vec(gauss_deg,max_pol_deg,xi)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                                         %
% fucntion dSF_dt_v = dSF_dt_vec(gauss_deg,max_pol_deg,xi)                                                %
%                                                                                                         %
% Inputs:                                                                                                 %
%  * gauss_deg: order of Gaussian quadrature                                                              %
%                                                                                                         %
%  * max_pol_deg: the maximum number of pol_deg that can have on element K_j,j=1,...,num_of_elem          %
%                                                                                                         %
%  * xi: Gaussian integration points                                                                      %
%                                                                                                         %
% Outputs:                                                                                                %
%                                                                                                         %
%  which evaluates the dSF_dt for all the xi pts from pol_deg=1,...,max_pol_deg                           %
%  , hence this program is used to speed up the computation for program                                   %
%  that need to calculated dSF_dt                                                                         %
%                                                                                                         %
% created by Yin Ching So (UNSW)                                                                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


for i=1:(max_pol_deg+2)
    
        dSF_dt_v(i,1:gauss_deg)=dSF_dt(xi(1:gauss_deg),i)';
       
end







