function y= dSF_dt(t,n)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% function  y= dSF_dt(t,n)                                              %
% evaluates the derivative of the values of the shape functions at t    %
%                                                                       %             
%     where dSF_dt=\phi_{n-1}^{'}(t),  i=1,2,......                     %
%                                                                       %
% Reference: B. Szabo' & I. Babuska, Finite Element Analysis, p. 38     %
%                                                           & p.98      %
%                                                                       %
% Y.C.So        18/10/05                                                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    switch n
        case 1
            phi=-1/2;
        case 2
            phi=1/2;
        otherwise
            if (n>1)
                phi=sqrt((2*(n-1)-1)/2).*(Legendre(t,n-2));
                
            else
                error('Polynomials not defined')
            end
    end
    y=phi;
    
    