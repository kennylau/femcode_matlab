function nc = node_connection(num_of_elem,num_of_node,num_of_side,t)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                                            
% function nc =node_connection(p,e,t)                                                                         
%                                                                                                            
% Inputs:                                                                                                    
%      
%     num_of_elem:
%     num_of_node:
%     num-of_side:
%     t:
%
% Outputs:                                                                                                 
%                                                                                                          
%  *nc: is a matrix with size max_connection by no.of vertex node on mesh                                  
%       , which stores the node index that connect to node i,i=1,.,no. of                                  
%       vertex nodes  
%  
%   by Yuxin SUN (UNSW)                                                                                                        
%                                                                        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%p = importdata('p.mat');
%e = importdata('e.mat');
%t = importdata('t.mat');

%  type = 3
%  num_of_elem = size(t,2);
%  num_of_side_node = size(e,2);
%  num_of_node = size(p,2);
%  num_of_side = type;
%  

nc = zeros(1,num_of_node);
nccount = ones(num_of_node);

for i = 1:num_of_side
    for j = 1:num_of_elem
        node = t(i,j); 
        nc(nccount(1,node),node)=j;
        nccount(1,node)=nccount(1,node)+1;
    end
end

                                                                                                                                                                                          