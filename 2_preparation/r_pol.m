function rp = r_pol(max_pol_deg,num_of_elem,ptype) 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                                         %
%  function rp = r_pol(max_pol_deg,num_of_elem,ptype)                                                     %
%                                                                                                         %
% Inputs:                                                                                                 %
%  * max_pol_deg: the maximum number of pol_deg that can have on element K_j,j=1,...,num_of_elem          %
%                                                                                                         %
%  * num_of_elem: number of element in the mesh                                                           %
%                                                                                                         %
%  * ptype: pol_deg type, 1 is randomly assign a pol_deg to element K_j                                   %
%                         2 is every element K_j have the same pol_deg = max_pol_deg                      %
%                                                                                                         %
% Outputs:                                                                                                %
%                                                                                                         %
%  *rp: is a vector which stores the pol degree of each element K_j,j=1,...,num_of_elem                   %
%                                                                                                         %                                                                                                         %
% created by Yin Ching So (UNSW)                                                                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


switch ptype
    case 0 % 'random'
        rp = ceil(max_pol_deg.*rand(num_of_elem,1));
    case 1 % 'uniform'
        rp = max_pol_deg*ones(num_of_elem,1);
    otherwise
        disp('Unknown type of pol_deg')
end