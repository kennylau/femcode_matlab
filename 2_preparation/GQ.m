function [Gpt,Gwt] = GQ

%--------------------------------------------------------------------%
% function [Gpt,Gwt] = GQ                                    %
% provides the integration points Gpt and weights Gwt                %
% of the Gaussian quadrature formula.                                %
% Output:                                                            %
%     Gpt(4,4) :  Gpt(:,i) is the Gaussian points of order i.        %
%     Gwt(4,4) :  Gwt(:,i) is the weights of quadrature of order i.  %
%                                                                    %
% Reference: B. Szabo and I. Babuska, Finite Element Analysis, p.356 %
%--------------------------------------------------------------------%

% Gaussian quadrature of order 1
Gpt(1,1) = 0;
Gwt(1,1)  = 2;		
% Gaussian quadrature of order 2
Gpt(1,2) = -1/sqrt(3);
Gpt(2,2) = -Gpt(1,2);
Gwt(1,2) = 1;
Gwt(2,2) = Gwt(1,2);		
% Gaussian quadrature of order 3
Gpt(1,3) = -sqrt(3/5);
Gpt(2,3) = 0;
Gpt(3,3) = -Gpt(1,3);
Gwt(1,3) = 5/9;
Gwt(2,3) = 8/9;
Gwt(3,3) = Gwt(1,3);	        
% Gaussian quadrature of order 4
Gpt(1,4) = - 0.861136311594053;
Gpt(2,4) = - 0.339981043584856;
Gpt(3,4) = -Gpt(2,4);
Gpt(4,4) = -Gpt(1,4);
Gwt(1,4) = 0.347854845137454;
Gwt(2,4) = 0.652145154862546;
Gwt(3,4) = Gwt(2,4);
Gwt(4,4) = Gwt(1,4);		
% Gaussian quadrature of order 5
Gpt(1,5) = - 0.906179845938664;
Gpt(2,5) = - 0.538469310105683;
Gpt(3,5) =   0.000000000000000;
Gpt(4,5) = -Gpt(2,5);
Gpt(5,5) = -Gpt(1,5);
Gwt(1,5) = 0.236926885056189;
Gwt(2,5) = 0.478628670499366;
Gwt(3,5) = 0.568888888888889;
Gwt(4,5) = Gwt(2,5);
Gwt(5,5) = Gwt(1,5);		
% Gaussian quadrature of order 6
Gpt(1,6) = - 0.932469514203152;
Gpt(2,6) = - 0.661209386466265;
Gpt(3,6) = - 0.238619186083197;
Gpt(4,6) = -Gpt(3,6);
Gpt(5,6) = -Gpt(2,6);
Gpt(6,6) = -Gpt(1,6);
Gwt(1,6) = 0.171324492379170;
Gwt(2,6) = 0.360761573048139;
Gwt(3,6) = 0.467913934572691;
Gwt(4,6) = Gwt(3,6);
Gwt(5,6) = Gwt(2,6);		
Gwt(6,6) = Gwt(1,6);		
% Gaussian quadrature of order 7
Gpt(1,7) = - 0.949107912342759;
Gpt(2,7) = - 0.741531185599394;
Gpt(3,7) = - 0.405845151377397;
Gpt(4,7) = - 0.000000000000000;
Gpt(5,7) = -Gpt(3,7);
Gpt(6,7) = -Gpt(2,7);
Gpt(7,7) = -Gpt(1,7);
Gwt(1,7) = 0.129484966168870;
Gwt(2,7) = 0.279705391489277;
Gwt(3,7) = 0.381830050505119;
Gwt(4,7) = 0.417959183673469;
Gwt(5,7) = Gwt(3,7);
Gwt(6,7) = Gwt(2,7);		
Gwt(7,7) = Gwt(1,7);		
% Gaussian quadrature of order 8
Gpt(1,8) = - 0.960289856497536;
Gpt(2,8) = - 0.796666477413627;
Gpt(3,8) = - 0.525532409916329;
Gpt(4,8) = - 0.183434642495650;
Gpt(5,8) = -Gpt(4,8);
Gpt(6,8) = -Gpt(3,8);
Gpt(7,8) = -Gpt(2,8);
Gpt(8,8) = -Gpt(1,8);
Gwt(1,8) = 0.101228536290376;
Gwt(2,8) = 0.222381034453374;
Gwt(3,8) = 0.313706645877887;
Gwt(4,8) = 0.362683783378362;
Gwt(5,8) = Gwt(4,8);
Gwt(6,8) = Gwt(3,8);		
Gwt(7,8) = Gwt(2,8);		
Gwt(8,8) = Gwt(1,8);		
% Gaussian quadrature of order 9
Gpt(1,9) = - 0.968160239507626;
Gpt(2,9) = - 0.836031107326636;
Gpt(3,9) = - 0.613371432700590;
Gpt(4,9) = - 0.324253423403809;
Gpt(5,9) = - 0.000000000000000;
Gpt(6,9) = -Gpt(4,9);
Gpt(7,9) = -Gpt(3,9);
Gpt(8,9) = -Gpt(2,9);
Gpt(9,9) = -Gpt(1,9);
Gwt(1,9) = 0.081274388361574;
Gwt(2,9) = 0.180648160694857;
Gwt(3,9) = 0.260610696402935;
Gwt(4,9) = 0.312347077040003;
Gwt(5,9) = 0.330239355001260;
Gwt(6,9) = Gwt(4,9);
Gwt(7,9) = Gwt(3,9);		
Gwt(8,9) = Gwt(2,9);		
Gwt(9,9) = Gwt(1,9);		
% Gaussian quadrature of order 10
Gpt(1,10)  = - 0.973906528517172;
Gpt(2,10)  = - 0.865063366688985;
Gpt(3,10)  = - 0.679409568299024;
Gpt(4,10)  = - 0.433395394129247;
Gpt(5,10)  = - 0.148874338981631;
Gpt(6,10)  = -Gpt(5,10);
Gpt(7,10)  = -Gpt(4,10);
Gpt(8,10)  = -Gpt(3,10);
Gpt(9,10)  = -Gpt(2,10);
Gpt(10,10) = -Gpt(1,10);
Gwt(1,10)  = 0.066671344308688;
Gwt(2,10)  = 0.149451349150581;
Gwt(3,10)  = 0.219086362515982;
Gwt(4,10)  = 0.269266719309996;
Gwt(5,10)  = 0.295524224714753;
Gwt(6,10)  = Gwt(5,10);
Gwt(7,10)  = Gwt(4,10);		
Gwt(8,10)  = Gwt(3,10);		
Gwt(9,10)  = Gwt(2,10);		
Gwt(10,10) = Gwt(1,10);		
