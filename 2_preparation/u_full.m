function [u_h,u_exact] = u_full(num_of_elem,gauss_deg,xi,u_c,rci_full,p,t,rp,rd,x_v,y_v,space)


u_h=[];
du_h_dx = [];
du_h_dy = [];
u_exact = [];

for l=1:num_of_elem
    
    Corner(1:4,:)=p(:,t(1:4,l))';
   
        
    for q=1:gauss_deg
        for qq=1:gauss_deg
            J=det_Jab(xi(q),xi(qq),Corner(:,1)',Corner(:,2)');
            dJ_v(q,qq)=J(1,1)*J(2,2)-J(1,2)*J(2,1);
            [u_approx_v(q,qq),du_approxdx(q,qq),du_approxdy(q,qq)]= u_approx(xi(q),xi(qq),J,l,u_c,rci_full,rp,rd);
                        
            u(q,qq)=coeff(x_v(l,q),y_v(l,qq),4);
        end
    end
    u_h     = [u_h,u_approx_v];
    du_h_dx = [du_h_dx,du_approxdx];
    du_h_dy = [du_h_dy,du_approxdy];
    u_exact = [u_exact,u];
end   