function rd= r_dof(rp,space)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                                         %
% fucntion  ri= r_dof(rp)                                                                                 %
%                                                                                                         %
% Inputs:                                                                                                 %
%                                                                                                         %
%  *rp: is a vector which stores the pol degree of each element K_j,j=1,...,num_of_elem                   %
%   space                                                                                                      %
% Outputs:                                                                                                %
%                                                                                                         %
%  *ri: which store the aggreciate dof of the elemnet                                                     %
%       hence , # of local dof of element i = ri(i+1)-ri(i);                                              %
%                                                                                                         %
% created by Yin Ching So (UNSW)  
% modified by Yuxin SUN (UNSW)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



num_of_elem=length(rp);
rd=zeros(num_of_elem+1,1);

for l=1:num_of_elem
    
    dof = basenum(rp(l),space); 
    rd(l+1)=rd(l)+dof;
    
    
end

              