function re = r_edge(ielem) % assume the orientation of this is positive

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                             %
% function re = r_edge(ielem,type)                                            %
%                                                                             %
% Inputs:                                                                     %
%                                                                             %
%  * ielem  = when itype is                                                   %
%                         2, ielem is the edg num=1,...,4                     %
%                                                                             %
%  *type (type of element) : triangle = 3                                     %
%                            quad     = 4                                     %
%                                                                             %
% Outputs:                                                                    %
%   * re: which give the vertex index of the edg ielem                        %
%                                                                             %
% modified by Yuxin SUN (UNSW)                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


 
        
        switch ielem
            case 1
                re=[1; 2];
            case 2
                re=[2;3];
            case 3
                re=[3; 4];
            case 4
                re=[4;1];
            otherwise
                disp('edge number is been exceed')
        end
   
            
 