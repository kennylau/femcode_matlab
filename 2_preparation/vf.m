function y = vf(a,c,t)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                   %
% function y = vf(a,c,t)                                            %
% defines a vector funciton from point a to point c, where          %
%  a,c \in R^2                                                      %
%                                                                   %
% Inputs:                                                           %
%                                                                   %
%   * a: coordinated of starting point                              %
%   * c: coordinated of end point                                   %
%   * t: time, where 0<=t<=1                                        %
% Outputs:                                                          %
%   * a vector valued on the ray when time is equal t               %
%                                                                   %
% created by Yin Ching So (UNSW)                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for i=1:length(t)
    y(i,:)=a+t(i).*(c-a);
end
