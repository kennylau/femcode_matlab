function [x,y] =T(eta,phi,X,Y) 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                                            %
% fucntion [x,y] =T(eta,phi,X,Y,ttype,type)                                                                  % 
%                                                                                                            %
% Inputs:                                                                                                    %
%  * eta: a value beteween [-1,1], which \in reference element [-1,1]^2                                      % 
%                                                                                                            %
%  * phi: a value beteween [-1,1], which \in reference element [-1,1]^2                                      %
%                                                                                                            %
%  * X: which stores the x-coordinates of the corner of the element K_j , i.e.  [X1 X2 X3 X4]  for quad      %
%                                                                                                            %
%  * Y: which stores the y-coordinates of the corner of the element K_j , i.e.  [Y1 Y2 Y3 Y4] for quad       %
%                                                                                                            %
%  * ttype = mapping type i.e                                                                                %
%            (1)   linear,                                                                                   %
%            (2)   quadratic parametric mapping (need convensional shape function)                           %
%            (3) Blending function method (using in p_version generally)                                     %
%                                                                                                            %
%                                                             %
%                                                                                                            %
% Outputs:                                                                                                   %
%                                                                                                            %
%  which computes a mapping T that map (eta,phi) \in \hat{R} to (x,y) \in Kj                                 %
%                                                                                                            %
% Reference: B. Szabo' & I. Babuska, Finite Element Analysis, p. 107                                         %
%                                                                                                            %
% created by Yin Ching So (UNSW)  
% modified by Yuxin SUN (UNSW) 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



                Ix(1:2,1:4)=[1 2 2 1;1 1 2 2];
                for i=1:4
                    N_v(i)= SF(eta,Ix(1,i))*SF(phi,Ix(2,i));
                end
                x=sum(N_v.*X);
                y=sum(N_v.*Y);
           
       
  
      
%x=((1-eta)/2)*xk+((1+eta)/2)*xkp1;
