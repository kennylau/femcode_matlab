function y= SF(t,n)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% function y=SF(t,n)                                                %
% evaluates the values of the shape functions                       %
%                                                                   %             
%     where SF(t,n)=\phi_{n-1}(t),  i=1,2,......                    %
%                                                                   %
% Reference: B. Szabo' & I. Babuska, Finite Element Analysis, p. 38 %
%                                                           & p.98  %
%                                                                   %
% Y.C.So        22/05/05                                            %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    switch n
        case 1
            phi=(1-t)/2;
        case 2
            phi=(1+t)/2;
        otherwise
            if (n>1)
                phi=(1/sqrt(2*(2*(n-1)-1))).*(Legendre(t,n-1)-Legendre(t,n-3));
                
            else
                error('Polynomials not defined')
            end
    end
    y=phi;
    
    