function n= search_edg(ielem,elem,rc,r)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                                         %
% fucntion n= search_edg(ielem,elem,rc,r,type)                                                            %
%                                                                                                         %
% Inputs:                                                                                                 %
%  * ielem  = when itype is                                                                               %
%                         2, ielem is the edg num=1,...,4                                                 %
%                                                                                                         %
%  * elem: the index of element K_j,j=1,..,num_of_elem                                                    %
%                                                                                                         %
%  *rc: is a matrix which stores the neighbour element index for each                                     %
%      element K_j, j=1,...,num_of_elem                                                                   %
%                                                                                                         %
%  * r: is the index of the corner vertex on element K_j,j=1,...,num_of_elem                              %
%                                                                                                         %
%  *type (type of element) : triangle = 3                                                                 %
%                            quad     = 4                                                                 %
%                                                                                                         %
% Outputs:                                                                                                %
%                                                                                                         %
%  *n: given the ielem of the elem, find the correspond ielem (edg num) of the neigh elem                 %
%      rc(l,elem) if it exist                                                                             %
%                                                                                                         %
% created by Yin Ching So (UNSW)                                                                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


re = r(r_edge(ielem),elem);
n=0;
% now generate the all the edg of the neighb
if(rc(ielem,elem)>0)
    for i=1:4
        ren=r(r_edge(i),rc(ielem,elem));
        if((re-ren)==0)|((re-[ren(2); ren(1)])==0)
            n=i;
            break
        end
    end
else
    disp('the ielm u enter for elem is a boudary edg')
end





