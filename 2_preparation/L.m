function z = L(x,y,index)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% function z = L(x,y,index)                                                   %
%                                                                             %    
% evaluates the nodal basis shape functions of reference triangle element     %
% with vertex(-1,0),(1,0) and (0,sqrt(3))                                     % 
%                                                                             %
% Inputs:                                           3                         %
%                                                   /\                        %
%   * x: vector of  input value                    /   \                      %
%   * index:   1,..3                              /      \                    %
%                                                /________\_                  %
% Outputs:                                     1             2                %
%   * the output value of nodal shape funciton on index                       %
%                                                                             %
%                                                                             %
%                                                                             %
% Reference: B. Szabo' & I. Babuska, Finite Element Analysis, p. 103          %
%                                                                             %
%                                                                             %
% Y.C.So        15/10/05                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


switch index
    case 1
        z=(1/2)*(1-x-(y/sqrt(3)));
    case 2
        z=(1/2)*(1+x-(y/sqrt(3)));
    case 3
        z=y/sqrt(3);
    otherwise
        disp('undefined index')
end
