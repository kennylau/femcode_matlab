function n = basenum(pol_deg,space)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                             %
%  function n = basenum(pol_deg,space)                                        %
%                                                                             %
% Inputs:                                                                     %
%                                                                             %
%   * pol_deg: polynomial degree used in FEM                                  %
%   * space: finite element subspace                                          %
%                                                                             %
%     where,                                                                  %
%      space 1 is Q_{p}^{'}                                                   %
%      space 2 is Q_{p}^{''}                                                  %
%      space 3 is Q_{p} 
%      space 4 is P_{p}
%      space 5 is S{\hat}_{p+1}                                                %
%                                                                             %
% Outputs:                                                                    %
%   * n: number of dof in element K_j,j=1,...,num_of_elem according to the    %
%         space                                                               %
% created by Yin Ching So (UNSW) 
% modified by Yuxin SUN (UNSW)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

p=pol_deg;
switch space
        case 1
            if (p>1)
               n=(p^2+3*p+6)/2;
           elseif (p==1)
               n=4;
           else
                disp('pol_deg is not define')
           end
        case 2
            if (p>1)
               n=(p^2+5*p+2)/2;
           elseif (p==1)
               n=4;
           else
                disp('pol_deg is not define')
           end
       case 3
           n =(p+1)^2;
       case 4
            n =(p+1)*(p+2)/2;
       case 5
            if (mod(p,2)==1)
                n=3;
            else
                n=1;
            end
       otherwise
           disp('space not define')
end