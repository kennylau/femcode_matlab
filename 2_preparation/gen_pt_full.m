function [x_full,y_full] = gen_pt_full(x_v,y_v,num_of_elem)

x_full = [];
y_full = [];

for i = 1:num_of_elem
    x = x_v(i,:);
    y = y_v(i,:);
    [xx,yy] = meshgrid(x,y);
    x_full = [x_full,xx];
    y_full = [y_full,yy];
end
