function SF_v1 = SF_vec1(SF_v)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                                         %
% fucntion SF_v1 = SF_vec1(max_pol_deg)                                                                   %
%                                                                                                         %
% Inputs:                                                                                                 %
%                                                                                                         %
%  * max_pol_deg: the maximum number of pol_deg that can have on element K_j,j=1,...,num_of_elem          %
%                                                                                                         %
%                                                                                                         %
% Outputs:                                                                                                %
%                                                                                                         %
%  which evaluates the SF for all the for -1 and 1  from pol_deg=1,...,max_pol_deg                        %
%  , hence this program is used to speed up the computation for program                                   %
%  that need to calculated SF for ER                                                                      %
%                                                                                                         %
% created by Yin Ching So (UNSW)                                                                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
SF_v1 = SF_v;
SF_v1(1,:)= ones;