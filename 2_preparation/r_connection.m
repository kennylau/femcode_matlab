function rc =r_connection(num_of_elem,num_of_side,nc,t)

rc = zeros(num_of_side,num_of_elem);
for l=1:num_of_elem
    for j=1:num_of_side
        re = r_edge(j);
       
        rc(j,l)=check_equal(nc(:,t(re(1),l)),nc(:,t(re(2),l)),l);
                
    end
end