function y=Legendre(x,n)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                   %
% function y=Legendre(x,n)                                          %
% defines a Legendre poly of order n                                %
%                                                                   %
%                                                                   %
% Inputs:                                                           %
%                                                                   %
%   * x: vector of  input value                                     %
%   * n: poly order n                                               %
%                                                                   %
% Outputs:                                                          %
%   * the output value of Legendre poly of degree n                 %
%                                                                   %
% created by Yin Ching So (UNSW) 
% modified by Yuxin SUN
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
switch n
    case 0
        y=1;
    case 1
        y=x;
    case 2
        y=(1/2)*(3*x.^2-1);
    case 3
        y=5/2*x.^3-(3/2)*x;
    case 4
        y=3/8+35/8*x.^4-15/4*x.^2;
    case 5
        y=63/8*x.^5-35/4*x.^3+15/8*x;
    case 6
        y=-5/16+231/16*x.^6-315/16*x.^4+105/16*x.^2;
    case 7
        y=429/16*x.^7-693/16*x.^5+315/16*x.^3-35/16*x;
    case 8
        y=6435/128*x.^8-12012/128*x.^6+6930/128*x.^4-1260/128*x.^2+35/128;
    otherwise
        error('Polynomials not defined')
end