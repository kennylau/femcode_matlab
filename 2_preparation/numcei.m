function num= numcei(itype,ielem,px,space)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                             %
% function num= numcei(itype,ielem,px,space,type)                             %
%                                                                             %
% Inputs:                                                                     %
%                                                                             %
%  * itype: 1 = corner                                                        %
%           2 = edge                                                          %
%           3 = interior                                                      %
%                                                                             %
%  * ielem  = when itype is 1, ielem is the corner num=1,...,4                %
%                         2, ielem is the edg num=1,...,4                     %
%                         3, ielem is the numbering of bubblem mode           %
%                                                                             %
%  * px: polynomial degree                                                    %
%                                                                             %
%  *type (type of element) : triangle = 3                                     %
%                            quad     = 4                                     %
%                                                                             %
% Outputs:                                                                    %
%                                                                             %
%   * num: number of dof correspond to the itype                              %
%                                                                             %
%                                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

num=0;
pol_deg=px;

switch itype
    case 1
        if (ielem<=4) 
            num=4;
        end
    case 2
        if (ielem<=4)
            num=pol_deg-1;
        end
        
    case 3
        switch space
            case 1
                if (pol_deg>1)
                    max_internal_dof=(pol_deg-2)*(pol_deg-3)/2;
                else
                    max_internal_dof=0;
                end
            case 2
                max_internal_dof=(pol_deg-1)*(pol_deg-2)/2;
            case 3
                max_internal_dof=(pol_deg-1)^2;
            case 4
                max_internal_dof=(pol_deg-1)*(pol_deg-2)/2;
            case 5
                if mod(pol_deg,2)==1
                    max_internal_dof = 3;
                else
                    max_internal_dof = 1;
                end
            otherwise
                disp('unkown space is choosen')
        end
   
        if (ielem<= max_internal_dof)
            num=max_internal_dof;
        end
    otherwise
          disp('invalid itype is choose')
end

        
        