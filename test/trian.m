function y = trian(x)
if x < 0
   y = sqrt(3)*(x+1);
else
   y = -sqrt(3)*(x-1);
end
