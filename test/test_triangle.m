x = linspace(-1,1);
y = (-1<=x&x<0)*sqrt(3).*(x+1)+(0<=x&x<=1)*sqrt(3).*(1-x);

N = size(x,2);

[xx,yy] = meshgrid(x,y);

newxx = zeros(size(xx));
newyy = zeros(size(yy));
newzz = zeros(size(xx));


zzl1 = 0.5*(1-xx-1/sqrt(3)*yy);
zzl2 = 0.5*(1+xx-1/sqrt(3)*yy);
zzl3 = yy*1/sqrt(3);

zzn12 = 0.5*(xx.^2-(1-1/sqrt(3)*yy).^2);
zzn13 = (1/6)*(xx.^3-xx*(1-1/sqrt(3)*yy).^2);  



for n=1:N;
    if n<=N/2,
        newzzl1(n:n,n:N-n+1) = newzz(n:n,n:N-n+1) + zzl1(n:n,n:N-n+1);
        newzzl2(n:n,n:N-n+1) = newzz(n:n,n:N-n+1) + zzl2(n:n,n:N-n+1);
        newzzl3(n:n,n:N-n+1) = newzz(n:n,n:N-n+1) + zzl3(n:n,n:N-n+1);
        newzzn12(n:n,n:N-n+1) = newzz(n:n,n:N-n+1) + zzn12(n:n,n:N-n+1);
    else
        newzzl1(n:n,N-n+1:n) = newzz(n:n,N-n+1:n) + zzl1(n:n,N-n+1:n);
        newzzl2(n:n,N-n+1:n) = newzz(n:n,N-n+1:n) + zzl2(n:n,N-n+1:n);
        newzzl3(n:n,N-n+1:n) = newzz(n:n,N-n+1:n) + zzl3(n:n,N-n+1:n);
        newzzn12(n:n,N-n+1:n) = newzz(n:n,N-n+1:n) + zzn12(n:n,N-n+1:n);
        
    end

end
n = n+1;  

mesh(xx,yy,newzzn12)
  



%     if mod(n,2)==1
%         newxx((n+1)/2:(N-(n+1)/2),n:n) = newxx((n+1)/2:(N-(n+1)/2),n:n) + xx((n+1)/2:(N-(n+1)/2),n:n);
%         newyy(n:n,(n+1)/2:(N-(n+1)/2)) = newyy(n:n,(n+1)/2:(N-(n+1)/2)) + yy(n:n,(n+1)/2:(N-(n+1)/2));
%     else
%         newxx((n/2+1):(N-(n/2+1)),n:n) = newxx((n/2+1):(N-(n/2+1)),n:n) + xx((n/2+1):(N-(n/2+1)),n:n);
%         newyy(n:n,(n/2+1):(N-(n/2+1))) = newyy(n:n,(n/2+1):(N-(n/2+1))) + yy(n:n,(n/2+1):(N-(n/2+1)));
%     end 
% 
% littlexx = zeros(10,10);
% littleyy = zeros(10,10);
% littlezz = ones(10,10);
% 
% for m=1:10
%     littlexx(1:10,m:m) = -1+(m-1)*(2/9);
%     littleyy(m:m,1:10) = (m-1)*(sqrt(3)/9);
% end
% m = m+1;
% 
% littlenewxx=zeros(10,10);
% littlenewyy=zeros(10,10);
% 
% for n=1:10
%     if n<=5
%         littlenewxx(n:n,n:N-n+1) = littlenewxx(n:n,n:N-n+1) + littlexx(n:n,n:N-n+1);
%         %littlenewyy(n:n,n:N-n+1) = littlenewyy(n:n,n:N-n+1) + littleyy(n:n,n:N-n+1);
%         %newyy(n:N-n+1,n:n) = newyy(n:N-n+1,n:n) + yy(n:N-n+1,n:n);
%     else
%         littlenewxx(n:n,N-n+1:n) = littlenewxx(n:n,N-n+1:n) + littlexx(n:n,N-n+1:n);
%         littlenewyy(n:n,N-n+1:n) = littlenewyy(n:n,N-n+1:n) + littleyy(n:n,N-n+1:n);
%         %newyy(N-n+1:n,n:n) = newyy(N-n+1:n,n:n) + yy(N-n+1:n,n:n);
%     end
% end
% n = n+1;
% 
% mesh(littlenewxx,littlenewyy,littlezz)
% 
% n = 1;
% for n=1:N
%     if n<=N/2
%         newxx(n:n,n:N-n+1) = newxx(n:n,n:N-n+1) + xx(n:n,n:N-n+1);
%         newyy(n:n,n:N-n+1) = newyy(n:n,n:N-n+1) + yy(n:n,n:N-n+1);
%         %newyy(n:N-n+1,n:n) = newyy(n:N-n+1,n:n) + yy(n:N-n+1,n:n);
%     else
%         newxx(n:n,N-n+1:n) = newxx(n:n,N-n+1:n) + xx(n:n,N-n+1:n);
%         newyy(n:n,N-n+1:n) = newyy(n:n,N-n+1:n) + yy(n:n,N-n+1:n);
%         %newyy(N-n+1:n,n:n) = newyy(N-n+1:n,n:n) + yy(N-n+1:n,n:n);
%     end
% end
% n = n+1;

%newzz = 0.5*(1-newxx-1/sqrt(3)*newyy);
%mesh(newxx,newyy,zz)
