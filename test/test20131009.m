function [e_L2,e_H1,E_L2,E_H1,eff_ind_L2, eff_ind_H1] = test20131009(max_pol_deg,meshtype,n)

%%%%%%%%%%%%%%%%%%%
% generate mesh
%%%%%%%%%%%%%%%%%%%
[p,e,t] = MESH_GENE([0,0],[1,0],[1,1],[0,1],n);

num_of_side = 4;
num_of_elem = size(t,2);
%num_of_side_node = size(e,2);
num_of_node = size(p,2);

bctype = 1; % only Dirichlet boundary condition considered
gauss_deg = 9;
[Gpt,Gwt] = GQ;
xi = Gpt(1:gauss_deg,gauss_deg);
w = Gwt(1:gauss_deg,gauss_deg);
[x_v,y_v] = gen_pt(num_of_elem,p,t,xi);
%[x_full,y_full] = gen_pt_full(x_v,y_v,num_of_elem);

%---------------------------------------------------
% evaluates the SF, dSF_dt for all the xi pts 
% from pol_deg=1,...,max_pol_deg, hence to speed up
%the computation
%---------------------------------------------------
SF_v = SF_vec(gauss_deg,max_pol_deg,xi);
SF_v1 = SF_vec1(max_pol_deg);
dSF_dt_v = dSF_dt_vec(gauss_deg,max_pol_deg,xi);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% use FEM to solve equation   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% if meshtype=4
%     space == 2;
% else 
%     space =4;
% end
space = 2;

%---------------------------------
% generate the global index
%---------------------------------

nc = node_connection(num_of_elem,num_of_node,num_of_side,t);
max_connection = size(nc,1);
rc =r_connection(num_of_elem,num_of_side,nc,t);

rp = max_pol_deg*ones(num_of_elem,1);
rd = r_dof(rp,space) ;

[rci,rcw] = global_index(num_of_elem,num_of_node,t,rc,rp,space,bctype);
rci_full = global_index_full(num_of_elem,num_of_node,t,rc,rp,rci,space,bctype);

%t1=cputime-t0
%-------------------------------------------------------------
% compute the global stiffness matrix, mass matrix,load matrix
%-------------------------------------------------------------
u_0 = u_zero(num_of_elem,p,t,rc,rp,rd,rci_full,space);
[GA,GA1,GF] = FORM_GA_GF(x_v,y_v,SF_v,dSF_dt_v,num_of_elem,gauss_deg,p,t,rci,rp,rd,xi,w,space);
F = main_rhs(x_v,y_v,SF_v,dSF_dt_v,GF,num_of_elem,gauss_deg,p,t,rci,rci_full,u_0,rp,rd,xi,w,space);
A = GA+GA1;

u_c=u_0;
u_c(1:max(rci))=A\F;

%[u_h,u_exact] = u_full(num_of_elem,gauss_deg,xi,u_c,rci_full,p,t,rp,rd,x_v,y_v,space);

%t2=cputime-t0
%mesh(x_full,y_full,u_h)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% A Posteriori Error Estimation        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[e_L2,e_H1] = error_L2H1(num_of_elem,x_v,y_v,u_c,p,t,rci_full,rp,rd,gauss_deg,xi,w);

[E_L2,E_H1] = error_E_e(num_of_elem,p,t,x_v,y_v,gauss_deg,max_pol_deg,SF_v,dSF_dt_v,xi,w,meshtype,rci_full,rp,rd,u_c);


eff_ind_L2=E_L2/e_L2;
eff_ind_H1=E_H1/e_H1;