% This script plots a function of 2 variables with the domain being the
% triangle with vertices (-1,0), (1,0), (0,sqrt(3)) in the xy plane
clear all
close all
M = 100;
% creates a mesh grid on the rectangle [-1,1]x[0,sqrt{3}]
x = linspace(-1,1,M);
y = linspace(0,sqrt(3),M);
[X,Y] = meshgrid(x,y);
% deletes points in mesh grid that are not in the interior of the triangle
l = 1;
for k = 1:M^2
   if Y(k) < trian(X(k))
      x1(l) = X(k);
      y1(l) = Y(k);
      l = l+1;
   end
end
% adds points on the sides of the triangles
for k = 1:M
   x1(l+k-1) = x(k);
   y1(l+k-1) = trian(x(k));
end
% plots points
figure(1)
plot(x1,y1,'*')
% creates triangles from points just created, then plots
tri = delaunay(x1,y1);
hold on
triplot(tri,x1,y1)
% plots the surface
figure(2)
z = N(x1,y1,3);
trimesh(tri,x1,y1,z)
%z = N(X,Y,2);
%cs = contour(X,Y,z);
%clabel(cs)
