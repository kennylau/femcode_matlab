function z = COEFF(x,y,ctype)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                                         
% fucntion z = COEFF(x,y,ctype)                                                                           
%                                                                                                         
% Inputs:                                                                                                 
%  * x: a x coordinated value which \in K_j,j=1,...,num_of_elem                                            
%                                                                                                         
%  * y: a y coordinated value which \in K_j,j=1,...,num_of_elem                                            
%                                                                                                         
%  * ctype: coeff type ,which                                                                             
%                       1,  a(x,y)                                                                        
%                       2,  b(x,y)                                                                        
%                       3,  f(x,y)                                                                        
%                       4,  g_D(x,y) and exact sol                                                       
%                                                                                         
%                       7,  d_u/d_x                                                                       
%                       8,  d_u/d_y                                                                       
%                                                                                                       
% Outputs:                                                                                                
%                                                                                                         
%  the function value correspond to the ctype                                                             
%                                                                                                         
%                                                                      
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global prob;

switch prob
    case 1
        switch ctype
            case 1
                z = 1;
            case 2
                z = 0;
            case 3
                z = -30*x^4 - 20*y^3;
            case 4
                z = x^6 + y^5;
            case 7
                z = 6*x^5;
            case 8
                z = 5*y^4;
            otherwise
                error('undefined ctype');
        end
    case 2
        switch ctype
            case 1
                z = 1;
            case 2
                z = 0;
            case 3
                z = -2*exp(x+y);
            case 4
                z = exp(x+y);
            case 7
                z = exp(x+y);
            case 8
                z = exp(x+y);
            otherwise
                error('undefined ctype');
        end
    case 3
        switch ctype
            case 1
                z = 1;
            case 2
                z = 0;
            case 3
                z = -(2*y + 4*x^2*y^2 + x^4)*exp(x^2*y);
            case 4
                z = exp(x^2*y);
            case 7
                z = 2*x*y*exp(x^2*y);
            case 8
                z = x^2*exp(x^2*y);
            otherwise
                error('undefined ctype');
        end
    case 4
        switch ctype
            case 1
                z = 1;
            case 2
                z = 0;
            case 3
                z = 5*pi^2*sin(2*pi*x)*sin(pi*y);
            case 4
                z = sin(2*pi*x)*sin(pi*y);
            case 7
                z = 2*pi*cos(2*pi*x)*sin(pi*y);
            case 8
                z = pi*sin(2*pi*x)*cos(pi*y);
            otherwise
                error('undefined ctype');
        end
    case 5
        switch ctype
            case 1
                z = 1;
            case 2
                z = 0;
            case 3
                z = (x^2 + y^2)*sin(x*y);
            case 4
                z = sin(x*y);
            case 7
                z = y*cos(x*y);
            case 8
                z = x*cos(x*y);
            otherwise
                error('undefined ctype');
        end
    case 6
        switch ctype
            case 1
                z = 1;
            case 2
                z = 0;
            case 3
                z = -(x^2 + y^2)*exp(-x*y);
            case 4
                z = exp(-x*y);
            case 7
                z = -y*exp(-x*y);
            case 8
                z = -x*exp(-x*y);
            otherwise
                error('undefined ctype');
        end
    case 7
        switch ctype
            case 1
                z = 1;
            case 2
                z = 0;
            case 3
                z = 2/(0.1+x+y)^2;
            case 4
                z = log(0.1+x+y);
            case 7
                z = 1/(0.1+x+y);
            case 8
                z = 1/(0.1+x+y);
            otherwise
                error('undefined ctype');
        end
    case 8
        switch ctype
            case 1
                z = 1;
            case 2
                z = 0;
            case 3
                %z = 2*((4*x^2 + 1)*tanh(x^2+y) - 1)*sech(x^2+y)^2 - 2*exp(x+y);
                z = -2*((y - x^2*(x^2 + 4*y^2)*tanh(x^2*y))*sech(x^2*y)^2 + exp(x+y));
            case 4
                z = tanh(x^2*y) + exp(x+y);
            case 7
                z = 2*x*y*sech(x^2*y)^2 + exp(x+y);
            case 8
                z = x^2*sech(x^2*y)^2 + exp(x+y);
            otherwise
                error('undefined ctype');
        end
    case 9
        switch ctype
            case 1
                z = x*y;
            case 2
                z = 0;
            case 3
                z = -(2*x*y + x + y)*exp(x+y);
            case 4
                z = exp(x+y);
            case 7
                z = exp(x+y);
            case 8
                z = exp(x+y);
            otherwise
                error('undefined ctype');
        end
    case 10
        switch ctype
            case 1
                z = exp(x)*y;
            case 2
                z = cos(4*pi*x);
            case 3
                z = -exp(x*y+x)*((x^2*y + x + y*(y^2 + y - 4*pi^2))*sin(2*pi*x) + 2*pi*y*(2*y + 1)*cos(2*pi*x)) + cos(4*pi*x)*sin(2*pi*x)*exp(x*y);
            case 4
                z = sin(2*pi*x)*exp(x*y);
            case 7
                z = (y*sin(2*pi*x) + 2*pi*cos(2*pi*x))*exp(x*y);
            case 8
                z = x*sin(2*pi*x)*exp(x*y);
            otherwise
                error('undefined ctype');
        end
    case 11
        switch ctype
            case 1
                z = sin(2*pi*x) + y + 1;
            case 2
                z = 0;
            case 3
                z = -(2*sin(2*pi*x) + 2*pi*cos(2*pi*x) + 2*y + 3)*exp(x+y);
            case 4
                z = exp(x+y);
            case 7
                z = exp(x+y);
            case 8
                z = exp(x+y);
            otherwise
                error('undefined ctype');
        end
    otherwise
        error('undefined problem');
end